<?php

$config = parse_ini_file( '/private/rsahub/config.ini', true );

$f3->set( 'dbhost', $config[ 'database' ][ 'dbhost' ] );
$f3->set( 'dbport', $config[ 'database' ][ 'dbport' ] );
$f3->set( 'dbuser', $config[ 'database' ][ 'dbuser' ] );
$f3->set( 'dbpass', $config[ 'database' ][ 'dbpass' ] );
$f3->set( 'dbname', $config[ 'database' ][ 'dbname' ] );

$f3->set( 'emailhost', $config[ 'email' ][ 'host' ] );
$f3->set( 'emailuser', $config[ 'email' ][ 'username' ] );
$f3->set( 'emailpass', $config[ 'email' ][ 'password' ] );

$f3->set( 'reticulum_host',         $config[ 'reticulum' ][ 'host' ] );
$f3->set( 'reticulum_header_name',  $config[ 'reticulum' ][ 'header_name' ] );
$f3->set( 'reticulum_header_value', $config[ 'reticulum' ][ 'header_value' ] );
$f3->set( 'reticulum_share_url',    $config[ 'reticulum' ][ 'share_root_url' ] );

$f3->set( 'api_host',   $config[ 'api' ][ 'host' ] );
$f3->set( 'api_port',   $config[ 'api' ][ 'port' ] );
$f3->set( 'api_secret', $config[ 'api' ][ 'secret' ] );

$aws_access_key_id     = $config[ 'aws' ][ 'access_key_id' ];
$aws_secret_access_key = $config[ 'aws' ][ 'secret_access_key' ];
putenv( "AWS_ACCESS_KEY_ID=$aws_access_key_id" );
putenv( "AWS_SECRET_ACCESS_KEY=$aws_secret_access_key" );
$f3->set( 'aws_region', $config[ 'aws' ][ 'region' ] );
$f3->set( 'aws_bucket', $config[ 'aws' ][ 'bucket' ] );
$f3->set( 'aws_url',    $config[ 'aws' ][ 'url' ] );

$f3->set( 'dashboard_base_url', $config[ 'dashboard' ][ 'base_url' ] );