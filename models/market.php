<?php

class Market extends DB\SQL\Mapper {

    public $selected;

    function __construct() {
        $this->selected = false;
        parent::__construct( \Base::instance()->get('DB'), 'markets' );    
    }

    public static function get_base_url( $bucket, $url = null ) {
        if( ! is_null( $url ) ) {
            return $url . Amazon_Storage_Handler::ROOT_FOLDER . Amazon_Storage_Handler::ASSETS_FOLDER;
        } else {
            return "https://$bucket.s3.amazonaws.com/" . Amazon_Storage_Handler::ROOT_FOLDER . Amazon_Storage_Handler::ASSETS_FOLDER;
        }
    }
}