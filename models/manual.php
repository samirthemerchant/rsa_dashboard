<?php

class Manual extends DB\SQL\Mapper {

    public $thumbnail, $src_url;

    function __construct( ) {
        parent::__construct( \Base::instance()->get( 'DB' ), 'manuals' );    
    }

    public function more_pages_exist( $current_page, $results_per_page ) { 
        $max_results = ( $current_page + 1) * $results_per_page;
        $total_rows = $this->count( 'delete_soon=0' );
        return $total_rows > $max_results;
    }

}