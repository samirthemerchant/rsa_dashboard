<?php

class Teacher extends DB\SQL\Mapper {

    const DEFAULT_PASSWORD_LENGTH = 8;

    public $selected;

    function __construct() {
        $selected = false;
        parent::__construct( \Base::instance()->get('DB'), 'teachers' );    
    }

    public function more_pages_exist( $current_page, $results_per_page ) { 
        $max_results = ( $current_page + 1) * $results_per_page;
        $total_rows = $this->count( 'delete_soon=0' );
        return $total_rows > $max_results;
    }

    public function generateRandomPassword() {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$&*=-';
        $pass = '';
        $charLength = strlen($chars) - 1;
        for ($i = 0; $i < self::DEFAULT_PASSWORD_LENGTH; $i++) {
            $n = rand(0, $charLength);
            $pass .= $chars[$n];
        }
        return $pass;
    }
}