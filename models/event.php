<?php

class Event extends DB\SQL\Mapper {

    public $short_start_date, $start_time, $short_end_date, $end_time;

    function __construct() {
        parent::__construct( \Base::instance()->get('DB'), 'events' );    
    }

    /**
     * Constant to represent available locations/counties
     */
    public static function get_locations( ) {
        return array(
            'Antrim',
            'Armagh',
            'Carlow',
            'Cavan',
            'Clare',
            'Cork',
            'Donegal',
            'Down',
            'Dublin',
            'Dun Laoghaire',
            'Fermanagh',
            'Fingal',
            'Galway',
            'Kerry',
            'Kildare',
            'Kilkenny',
            'Laois',
            'Leitrim',
            'Limerick',
            'Londonderry',
            'Longford',
            'Louth',
            'Mayo',
            'Meath',
            'Monaghan',
            'North Tipperary',
            'Offaly',
            'Roscommon',
            'Sligo',
            'South Dublin',
            'South Tipperary',
            'Tipperary',
            'Tyrone',
            'Waterford',
            'Westmeath',
            'Wexford',
            'Wicklow',
        );
    }

    public function is_teacher_selected( $teacher ) {
        $teacher->selected = $teacher->id == $this->teacher_id;
        return $teacher;
    }

    public function is_market_selected( $market ) {
        $market->selected = $market->id === $this->market_id;
        return $market;
    }

    public function more_pages_exist( $current_page, $results_per_page, $filter ) { 
        $max_results = ( $current_page + 1) * $results_per_page;
        $total_rows = $this->get_event_list_details( $filter );
        return count( $total_rows ) > $max_results;
    }

    public function get_started_events( ) {
        $open_time = time() + ( 60 * 60 * 24 );
        $now = date( 'Y-m-d H:i:s', $open_time );
        return $this->db->exec(
            'SELECT e.id, e.title, e.market_id, m.scene_id, t.account_id 
            FROM events AS e JOIN markets AS m 
                ON e.market_id = m.id
            JOIN teachers AS t 
                ON e.teacher_id = t.id
            WHERE e.start_date < ? AND e.hub_id IS NULL 
                AND e.url IS NULL AND e.delete_soon = 0
            ORDER BY e.start_date ASC',
            $now
        );
    }

    public function get_event_list_details( $filter, $limit = false, $offset = false ) {
        $query = 'SELECT e.id, e.title, e.start_date, e.end_date, e.location, 
                    m.name AS market_name, t.name AS teacher_name  
                FROM events AS e JOIN markets AS m JOIN teachers AS t
                ON e.market_id = m.id AND e.teacher_id = t.id';
        $params = array();

        if( ! is_null( $filter ) ) {
            $query .= ' WHERE e.' . $filter[ 0 ];
            $query .= ' AND e.delete_soon = 0';
            $params[] = $filter[ 1 ];
        } else {
            $query .= ' WHERE e.delete_soon = 0';
        }

        $query .= ' ORDER BY e.date_modified DESC, e.date_created DESC, e.start_date DESC, e.location ASC, e.title ASC, e.end_date ASC';

        if( false !== $limit && false !== $offset ) {
            $query .= ' LIMIT ? OFFSET ?';
            $params[] = $limit;
            $params[] = $offset;
        }

        return $this->db->exec( $query, $params );
    }

    public function get_event_details( $base_url = null, $share_url = null ) {
        $event_details = $this->db->exec(
            'SELECT e.title, e.start_date, e.end_date, e.hub_id, e.url, e.use_temp_urls, 
                m.name AS market_name 
            FROM events AS e JOIN markets AS m 
            ON e.market_id = m.id 
            WHERE e.id = ? AND e.delete_soon = 0 LIMIT 1',
            $this->id
        );
        $event_detail = $event_details[ 0 ];

        if( intval( $event_detail[ 'use_temp_urls' ] ) ) {
            $event_detail[ 'share_url' ] = "$base_url/presenters/lobby/" . $this->id;
        } else {
            if( $event_detail[ 'hub_id' ] !== null && $share_url !== null ) {
                // $event_detail[ 'share_url' ] = $share_url . $event_detail[ 'hub_id' ];
                $event_detail[ 'share_url' ] = $event_detail[ 'url' ];
            } else {
                $event_detail[ 'share_url' ] = null;
            }
        }
        return $event_detail; 
    }

    public function get_event_teacher( ) {
        $teacher = new Teacher();
        $teacher->load( array( 'id=? AND delete_soon=0', $this->teacher_id ) );
        return $teacher;
    }

    private function is_not_empty( $var ) {
        return ! empty( $var );
    }
}