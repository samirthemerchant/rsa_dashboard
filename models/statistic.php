<?php

class Statistic extends DB\SQL\Mapper {

    public $asset_url;

    function __construct() {
        parent::__construct( \Base::instance()->get('DB'), 'statistics' );    
        $asset_url = null;
    }

    function set_src_url( $bucket, $url = null ) {
        if( ! is_null( $this->s3_key ) ) {
            if( ! is_null( $url ) ){
                $this->asset_url = $url . $this->s3_key;
            } else {
                $this->asset_url =  "https://$bucket.s3.amazonaws.com/" . $this->s3_key;
            }
        }
        return;
    }
}