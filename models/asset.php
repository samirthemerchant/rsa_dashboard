<?php

class Asset extends DB\SQL\Mapper {

    const IMG_DIR        = '/assets/img/';
    const SETTINGS_TABLE = 'market_assets';

    public $thumbnail, $src_url;

    function __construct( ) {
        parent::__construct( \Base::instance()->get( 'DB' ), 'assets' );    
    }

    /**
     * Constant to represent available asset types
     */
    public static function get_asset_types() {
        return array(
            'Roundabout Billboard',
            'Village Billboard',
            'Rural Billboard',
            'City Billboard',
            'Coastal Billboard'
        );
    }

    public static function get_src_url( $bucket, $asset, $url = null ) {
        if( ! is_null( $url ) ) {
            return $url . $asset[ 's3_key' ];
        } else {
            return "https://$bucket.s3.amazonaws.com/" . $asset[ 's3_key' ];
        }
    }

    public static function get_thumbnail( $bucket, $asset, $url = null ) {
        $extension = pathinfo( $asset[ 'filename' ], PATHINFO_EXTENSION );
        $thumbnail = '';

        switch( $extension ) {
            case 'gif':
            case 'jpeg':
            case 'jpg':
            case 'png':
                $thumbnail = self::get_src_url( $bucket, $asset, $url );
                break;  

            case 'pdf':
                $thumbnail = \Base::instance()->get( 'BASE' ) . self::IMG_DIR . 'pdf-icon.png';
            break;
            
            case 'mp4':
                $thumbnail = \Base::instance()->get( 'BASE' ) . self::IMG_DIR . 'mp4-icon.png';
            break;
            
            default:
                $thumbnail = \Base::instance()->get( 'BASE' ) . self::IMG_DIR . 'file-icon.png';
                break;
        }

        return $thumbnail;
    }

    public function get_asset_details_for_market( $market_id, $bucket, $cdn_url ) {
        $settings = self::SETTINGS_TABLE;
        $assets = array();

        $market_assets = $this->db->exec(
            "SELECT s.asset_type, a.filename 
            FROM $settings AS s JOIN assets AS a 
            ON s.asset_id = a.id 
            WHERE s.market_id=? AND s.is_selected=1
                AND a.delete_soon=0
            ORDER BY s.asset_type ASC",
            $market_id
        );

        $amazon = new Amazon_Storage_Handler();
        foreach( $market_assets as $asset ) {
            $extension = pathinfo( $asset[ 'filename' ], PATHINFO_EXTENSION );
            $asset[ 's3_key' ] = $amazon->get_market_key_name( 
                $market_id, 
                $asset[ 'asset_type' ], 
                $extension
            );
            
            $assets[] = array(
                'type' => intval( $asset[ 'asset_type' ] ),
                'url' => self::get_src_url( $bucket, $asset, $cdn_url ),
                'extension' => $extension
            );
        }
        return $assets;
    }

    public function get_videos_for_market( $market_id ) {
        $types_with_video = array();
        $settings = self::SETTINGS_TABLE;

        $market_assets = $this->db->exec(
            "SELECT s.asset_type, a.filename 
            FROM $settings AS s JOIN assets AS a 
            ON s.asset_id = a.id 
            WHERE s.market_id=? AND s.is_selected=1
                AND a.delete_soon=0",
            $market_id
        );

        foreach( $market_assets as $asset ) {
            $extension = pathinfo( $asset[ 'filename' ], PATHINFO_EXTENSION );
            if( 'mp4' == $extension ) {
                $types_with_video[] = $asset[ 'asset_type' ];
            }
        }

        return $types_with_video;
    }


    public function more_pages_exist( $current_page, $results_per_page ) { 
        $max_results = ( $current_page + 1) * $results_per_page;
        $total_rows = $this->count( 'delete_soon=0' );
        return $total_rows > $max_results;
    }


    /**
     * Get list of all assets currently used in a market
     * for a particular asset
     */
    public function get_selected_market_assets() {
        $settings = self::SETTINGS_TABLE;
        return $this->db->exec(
            "SELECT market_id, asset_type FROM $settings WHERE asset_id=? AND is_selected=1",
            $this->id 
        );
    }

    /**
     * Edit asset utility function
     * Get array of ids for markets and types
     */
    public function get_asset_settings() {
        $settings = self::SETTINGS_TABLE;
        $market_ids = $this->db->exec(
            "SELECT market_id AS row_key FROM $settings WHERE asset_id=? GROUP BY market_id",
            $this->id
        );
        $asset_types = $this->db->exec(
            "SELECT asset_type AS row_key FROM $settings WHERE asset_id=? GROUP BY asset_type",
            $this->id
        );
        return array( 
            array_map( array( $this, 'flatten' ), $market_ids ), 
            array_map( array( $this, 'flatten' ), $asset_types ) 
        );
    }

    /**
     * Update asset utility function
     * Store new asset settings
     */
    public function set_asset_settings( $market_ids, $type ) {
        if( ! is_array( $market_ids ) || ! is_numeric( $type ) ) {
            throw new Exception( 'Target markets or types not in correct format' );
        }
        // Reset settings for asset
        $settings = self::SETTINGS_TABLE;
        $reset_sql = "DELETE FROM $settings WHERE asset_id=?";
        $this->db->exec( $reset_sql, $this->id );

        // Update settings for asset
        $placeholders = array();
        $values = array();
        $update_sql = "INSERT INTO $settings (market_id, asset_id, asset_type) VALUES "; 
        foreach( $market_ids as $id ) {
            $placeholders[] = '(?,?,?)';
            $values[] = intval( $id );
            $values[] = $this->id;
            $values[] = intval( $type );
        }
        $update_sql .= implode( ',', $placeholders );
        $this->db->exec( $update_sql, $values );
    }

    /**
     * Edit market utility function
     * Get available assets grouped by type
     */
    public function get_assets_for_market( $market_id, $bucket, $url ) {
        $asset_types = self::get_asset_types();
        $settings = self::SETTINGS_TABLE;
        $sql = "SELECT a.asset_type, a.is_selected, 
                    b.id, b.name, b.filename, b.s3_key 
                FROM $settings AS a JOIN assets AS b 
                ON a.asset_id = b.id 
                WHERE a.market_id = ? AND b.delete_soon = 0
                ORDER BY a.asset_type ASC, b.name ASC, a.asset_id ASC";
        $assets = $this->db->exec( $sql, $market_id );

        $market_assets = array();
        foreach( $assets as $asset ) {
            $asset[ 'thumbnail' ] = self::get_thumbnail( $bucket, $asset, $url );
            $asset[ 'src_url' ]   = self::get_src_url( $bucket, $asset, $url );

            if( isset( $market_assets[ $asset[ 'asset_type' ] ] ) ){
                $market_assets[ $asset[ 'asset_type' ] ][] = $asset;
            } else {
                $market_assets[ $asset[ 'asset_type' ] ] = array( $asset );
            }
        }
        return $market_assets;
    }

    /**
     * Update market utility function
     * Set asset as selected for market
     */
    public function set_asset_for_market( $market_id, $asset_type, $asset_id ) {
        $settings = self::SETTINGS_TABLE;
        // Reset values for market asset type
        $reset_sql = "UPDATE $settings 
                      SET is_selected = 0 
                      WHERE market_id = ? AND asset_type = ?";
        $this->db->exec( 
            $reset_sql,
            array( $market_id, $asset_type )
        );

        // Update values for market asset type
        $update_sql = "UPDATE $settings 
                       SET is_selected = 1 
                       WHERE market_id = ? AND asset_type = ? 
                           AND asset_id = ?
                       LIMIT 1";
        $this->db->exec(
            $update_sql,
            array( $market_id, $asset_type, $asset_id )
        );
    }

    /**
     * Erase accompanying settings when asset is erased
     */
    public function erase_settings() {
        $settings = self::SETTINGS_TABLE;
        $this->db->exec( 
            "DELETE FROM $settings WHERE asset_id = ?",
            $this->id
        );
    }

    private function flatten( $obj ) {
        return $obj[ 'row_key' ];
    }
}