<?php

class Admins {

    const CURFEW_START_TIME = 23,
        CURFEW_END_TIME = 5;// 23:00 - 05:55

    /**
     * Redirect splash page
     */
    public function redirectSplash( $f3 ) {
        if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@teacher_home' );
        } else {
            $f3->reroute( '@admin_home' );
        }
    }

    /**
     * Initial check to confirm whether 2FA has been configured correctly.
     */
    public static function check2FAConfigured( $f3, $email = null ) {
        if ( ! $email ) {
            $email = $f3->get( 'SESSION.admin_email' );
        }
        $admin = new Admin();
        $admin->load( array( 'email=?', $email ) );
        if ( 1 !== $admin->g2fa_enabled ) {
            $f3->set( 'SESSION.error', 'You must register your device for two-factor authentication before continuing' );
            $f3->reroute( '@admin_edit_2fa' );
        }
    }

    /**
     * @admin_show_2fa
     * 
     * Admin 2FA Login Form
     */
    public function show_2fa( $f3 ) {
        if ( is_null( $f3->get( 'SESSION.login_email' ) ) ) {
            $f3->reroute( '@admin_home' );
        }
        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }

        
        $admin = new Admin();
        $email = $f3->get( 'SESSION.login_email' );
        self::check2FAConfigured( $f3, $email );
        $admin->load( array( 'email=?', $email ) );
        if( $admin->dry() ) {
            // Session data is corrupted somehow
            throw new Exception( 'Could not find user. Please login again.' );
        }
        if ( Authenticator::MAX_ATTEMPTS <= $admin->g2fa_attempts ) {
            $admin->g2fa_attempts = 0;
            $admin->g2fa_timestamp = date( 'Y-m-d H:i:s' );
            $admin->save();

            $f3->set( 'SESSION.error', 'Please try again later.' );
            $f3->clear( 'SESSION.login_email' );
            $f3->reroute( '@admin_login' );
        }

        $f3->set( 'title', 'Complete login' );

        echo \Template::instance()->render( 'g2fa.htm' );
    }

    /**
     * @admin_verify_2fa
     * 
     * Admin Verify 2FA Challenge
     */
    public function verify_2fa( $f3 ) {
        if ( is_null( $f3->get( 'SESSION.login_email' ) ) ) {
            $f3->reroute( '@admin_home' );
        }
        $email = $f3->get( 'SESSION.login_email' );
        self::check2FAConfigured( $f3, $email );

        try {
            $validator = new Validator();
            list( $code ) = $validator->getValues( array( 'code' ), $f3->get( 'POST' ) );
            $validator->checkEmpty( array( $code ) );

            $admin = new Admin();
            $admin->load( array( 'email=?', $email ) );
            if( $admin->dry() ) {
                // Session data is corrupted somehow
                throw new Exception( 'Could not find user. Please login again.' );
            }

            $authenticator = new Authenticator();
            $is_valid = $authenticator->verify_auth_code( $admin, $code );

            if ( ! $is_valid ) {
                $admin->g2fa_attempts++;
                $admin->save();
                throw new Exception( "Code is not valid!" );
            }

            // Set cookies
            $f3->set( 'SESSION.admin_email', $admin->email );
            $f3->set( 'SESSION.admin_name', $admin->name );
            $f3->clear( 'SESSION.login_email' );
            
            $admin->g2fa_attempts = 0;
            $admin->save();

            $f3->reroute( '@admin_home' );
        } catch ( Exception $e ) {
            $f3->set( 'SESSION.error', $e->getMessage() );
            $f3->reroute( '@admin_show_2fa' );
        }

    }

    /**
     * @admin_edit_2fa
     * 
     * Admin Configure 2FA Device
     */
    public function edit_2fa( $f3 ) {
        if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@admin_login' );
        }
        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }
        if( ! is_null( $f3->get( 'SESSION.success' ) ) ) {
            $f3->set( 'success', $f3->get( 'SESSION.success' ) );
            $f3->clear( 'SESSION.success' );
        }

        $admin = new Admin();
        $email = $f3->get( 'SESSION.admin_email' );
        $admin->load( array( 'email=?', $email ) );
        if( $admin->dry() ) {
            // Session data is corrupted somehow
            throw new Exception( 'Could not find user. Please login again.' );
        }

        $authenticator = new Authenticator();
        $qr_code = $authenticator->get_qr_code( $admin );

        $f3->set( 'qr_code', $qr_code );
        $f3->set( 'title', 'Configure 2FA' );
        $f3->set( 'g2fa_enabled', $admin->g2fa_enabled );

        echo \Template::instance()->render( 'admins/configure_2fa.htm' );
    }

    /**
     * @admin_update_2fa
     * 
     * Admin Update 2FA Device
     */
    public function update_2fa( $f3 ) {
        if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@admin_login' );
        }

        try {
            $validator = new Validator();
            list( $code ) = $validator->getValues( array( 'code' ), $f3->get( 'POST' ) );
            $validator->checkEmpty( array( $code ) );

            $admin = new Admin();
            $email = $f3->get( 'SESSION.admin_email' );
            $admin->load( array( 'email=?', $email ) );
            if( $admin->dry() ) {
                // Session data is corrupted somehow
                throw new Exception( 'Could not find user. Please login again.' );
            }

            $authenticator = new Authenticator();
            $is_valid = $authenticator->verify_auth_code( $admin, $code );

            if ( $is_valid ) {
                $admin->g2fa_enabled = 1;
                $admin->save();
            } else {
                throw new Exception( 'Code is not valid!' );
            }

            $f3->set( 'SESSION.success', 'Two-factor authentication has been enabled!' );

            $f3->reroute( '@admin_home' );
        } catch ( Exception $e ) {
            $f3->set( 'SESSION.error', $e->getMessage() );
            $f3->reroute( '@admin_edit_2fa' );
        }
    }

    /**
     * @admin_reset_2fa
     * 
     * Admin Reset 2FA Configuration
     */
    public function reset_2fa( $f3 ) {
        if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@admin_login' );
        }

        try {
            $admin = new Admin();
            $email = $f3->get( 'SESSION.admin_email' );
            $admin->load( array( 'email=?', $email ) );
            if( $admin->dry() ) {
                // Session data is corrupted somehow
                throw new Exception( 'Could not find user. Please login again.' );
            }
            $admin->g2fa_secret = null;
            $admin->g2fa_enabled = 0;
            $admin->save();


            $f3->set( 'SESSION.success', 'Two-factor authentication has been reset' );
            $f3->reroute( '@admin_edit_2fa' );
        } catch ( Exception $e ) {
            $f3->set( 'SESSION.error', $e->getMessage() );
            $f3->reroute( '@admin_edit_2fa' );
        }
    }

    /**
     * @admin_login
     * 
     * Admin dashboard login page
     */
    public function login( $f3 ) {
        if( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@admin_home' );
        }
        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }

        $session = $f3->get( 'session' );
        $f3->set( 'CSRF', $session->csrf() );
        $f3->copy( 'CSRF', 'SESSION.csrf' );

        $f3->set( 'users', 'Admins' );
        $f3->set( 'route', 'admin_check_login' );
        $f3->set( 'title', 'Admin' );
        echo \Template::instance()->render( 'login.htm' );   
    }
    /**
     * @admin_check_login
     * 
     * Check admin login credentials
     */
    public function check_login( $f3 ) {
        if( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@admin_home' );
        }

        try {
            $token = $f3->get( 'POST.token' );
            $csrf = $f3->get( 'SESSION.csrf' );
            if ( empty($token) || empty($csrf) || $token !== $csrf ) {
                // CSRF attack detected
                $f3->error( 403 );
            }

            $validator = new Validator();
            list( $email, $password ) = $validator->getValues( 
                array( 'email', 'password' ), 
                $f3->get( 'POST' ) 
            );
            $validator->checkEmail( array( $email ) );
            $validator->checkEmpty( array( $password ) );

            $admin = new Admin();
            $admin->load( array( 'email=?', strtolower( $email ) ) );
            if( $admin->dry() ) {
                throw new Exception( 'Email or password is incorrect' );
            } else if( ! password_verify( $password, $admin->password ) ) {
                throw new Exception( 'Email or password is incorrect' );
            }

            if ( 1 === $admin->g2fa_enabled ) {
                if ( ! is_null( $admin->g2fa_timestamp ) ) {
                    if ( time() - strtotime( $admin->g2fa_timestamp ) < Authenticator::MAX_TIMEOUT ) {
                        throw new Exception( 'Please try again later' );
                    } else {
                        $admin->g2fa_timestamp = null;
                        $admin->save();
                    }
                }

                $f3->set( 'SESSION.login_email', $admin->email );
                $f3->reroute( '@admin_show_2fa' );
            }

            // Set cookies
            $f3->set( 'SESSION.admin_email', $admin->email );
            $f3->set( 'SESSION.admin_name', $admin->name );

            $f3->reroute( '@admin_home' );
        } catch( Exception $e ) {
            $f3->set( 'SESSION.error', $e->getMessage() );
            $f3->reroute( '@admin_login' );
        }
    }
    
    /**
     * @admin_logout
     * 
     * Logout admin
     */
    public function logout( $f3 ) {
        if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@admin_login' );
        }

        $f3->clear( 'SESSION' );
        $f3->reroute( '@admin_login' );
    }

    /**
     * @admin_password
     * 
     * Change admin password form
     */
    public function change_password( $f3 ) {
        if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@admin_login' );
        }

        self::check2FAConfigured( $f3 );

        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }
        $f3->set( 'users', 'Admins' );
        $f3->set( 'route', 'admin_update_password' );
        $f3->set( 'title', 'Change Password' );
        echo \Template::instance()->render( 'password.htm' );   
    }
    /**
     * @update_admin_password
     * 
     * Update admin password
     */
    public function update_password( $f3 ) {
        if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@admin_login' );
        }

        self::check2FAConfigured( $f3 );

        try {
            $validator = new Validator();
            list( $old_password, $new_password, $confirm_password ) = $validator->getValues( 
                array( 'old_password', 'new_password', 'confirm_password' ),
                $f3->get( 'POST' )
            );
            $validator->checkEmpty( array( $old_password, $new_password, $confirm_password ) );
            // TODO: Check new password length, contains numbers, special chars, etc.

            $admin = new Admin();
            $email = $f3->get( 'SESSION.admin_email' );
            $admin->load( array( 'email=?', $email ) );
            if( $admin->dry() ) {
                // Session data is corrupted somehow
                throw new Exception( 'Could not find user. Please login again.' );
            } else if( ! password_verify( $old_password, $admin->password ) ) {
                throw new Exception( 'Old password is incorrect' );
            } else if( $new_password !== $confirm_password ) {
                throw new Exception( 'New passwords do not match' );
            }

            $admin->password = password_hash( $new_password, PASSWORD_DEFAULT );
            $admin->save();
            $f3->set( 'SESSION.success', 'Password updated!' );
            
            $f3->reroute( '@admin_home' );
        } catch( Exception $e ) {
            $f3->set( 'SESSION.error', $e->getMessage() );
            $f3->reroute( '@admin_password' );
        }

    }

    /**
     * @admin_home
     * 
     * Admins dashboard main page
     */
    public function home( $f3 ) {
        if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@admin_login' );
        }

        self::check2FAConfigured( $f3 );

        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }
        if( ! is_null( $f3->get( 'SESSION.success' ) ) ) {
            $f3->set( 'success', $f3->get( 'SESSION.success' ) );
            $f3->clear( 'SESSION.success' );
        }

        $f3->set( 'users', 'Admins' );
        $f3->set( 'name', $f3->get( 'SESSION.admin_name' ) );
        $f3->set( 'title', 'Home' );
        echo \Template::instance()->render( 'admins/home.htm' );
    }


    /**
     * @admin_close_rooms
     * 
     * Force close active Hubs rooms
     */
    public function force_close_rooms( $f3 ) {
        if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@admin_login' );
        }

        self::check2FAConfigured( $f3 );

        try {
            $reticulum_host         = $f3->get( 'reticulum_host' );
            $reticulum_header_name  = $f3->get( 'reticulum_header_name' );
            $reticulum_header_value = $f3->get( 'reticulum_header_value' );
            $api_host = $f3->get( 'api_host' );
            $api_port = $f3->get( 'api_port' );
            $api_secret = $f3->get( 'api_secret' );
    
            $hub = new Hub_Generator( $reticulum_host, $api_host, $api_port, $api_secret );
            $hub->set_admin_header( $reticulum_header_name, $reticulum_header_value );
            $rooms = $hub->get_active_rooms();
            $success = 0;
            $errors = 0;
            foreach( $rooms as $room ) {
                $hub_id = $room[ 'hub_sid' ];
                $status_code = $hub->close( $hub_id );
                if( '200' == $status_code ) {
                    $success++;
                } elseif( '403' == $status_code ) {
                    // Avoid server timeout
                    sleep( 1 );
                    $status_code = $hub->close( $hub_id );
                    if( '200' == $status_code ) {
                        $success++;
                    } else {
                        $errors++;
                    }
                } else {
                    $errors++;
                }
            }
        
            if( 0 < $success ) {
                $f3->set( 'SESSION.success', "$success active rooms were closed!" );
            }
            if( 0 < $errors ) {
                $f3->set( 'SESSION.error', "$errors active rooms could not be closed!" );
            }
            $f3->reroute( '@admin_home' );
        } catch( Exception $e ) {
            $f3->set( 'SESSION.error', $e->getMessage() );
            $f3->reroute( '@admin_home' );
        }
    }

    /**
     * Close all active rooms if within curfew
     */
    public function enforce_event_curfew( $f3 ) {
        $current_hour = intval( date( 'G') );
        if( $current_hour > self::CURFEW_END_TIME && $current_hour < self::CURFEW_START_TIME ) {
            // Currently not in curfew
            return;
        }
        
        $logger = new Logger( __DIR__ . '/../scripts/cron.log' );
        $logger->message( 'CRON', null, 'Starting cron task: `check_event_curfew`...');

        $event = new Event();
        if( $event->count( 'hub_id IS NOT NULL' ) ) {
            // There are currently active events
            $logger->error( 'CRON', null, 'Found active events within curfew!' );
            $logger->message( 'CRON', null, 'Ending cron task: `check_event_curfew`.');
            return;
        }

        $reticulum_host         = $f3->get( 'reticulum_host' );
        $reticulum_header_name  = $f3->get( 'reticulum_header_name' );
        $reticulum_header_value = $f3->get( 'reticulum_header_value' );
        $api_host = $f3->get( 'api_host' );
        $api_port = $f3->get( 'api_port' );
        $api_secret = $f3->get( 'api_secret' );

        $hub = new Hub_Generator( $reticulum_host, $api_host, $api_port, $api_secret );
        $hub->set_admin_header( $reticulum_header_name, $reticulum_header_value );
        $rooms = $hub->get_active_rooms();
        $success = 0;
        $errors = 0;
        foreach( $rooms as $room ) {
            $hub_id = $room[ 'hub_sid' ];
            $logger->message( 'Hub', $hub_id, 'Closing Hubs room...' );
            
            $status_code = $hub->close( $hub_id );
            if( '200' == $status_code ) {
                $logger->message( 'Hub', $hub_id, 'Hubs room closed successfully.' );
                $success++;
            } elseif( '403' == $status_code ) {
                // Avoid server timeout
                sleep( 1 );
                $status_code = $hub->close( $hub_id );
                if( '200' == $status_code ) {
                    $logger->message( 'Hub', $hub_id, 'Hubs room closed successfully.' );
                    $success++;
                } else {
                    $logger->error( 'Hub', $hub_id, "Could not close room! Bad response from Hubs server - $status_code" );
                    $errors++;
                }
            } else {
                $logger->error( 'Hub', $hub_id, "Could not close room! Bad response from Hubs server - $status_code" );
                $errors++;
            }
        }

        $logger->message( 'CRON', null, "$success active rooms were closed!" );
        $logger->message( 'CRON', null, "$errors active rooms could not be closed!" );
        $logger->message( 'CRON', null, 'Ending cron task: `check_event_curfew`.');
    }
}