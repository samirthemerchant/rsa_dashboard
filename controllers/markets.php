<?php

class Markets {

    // User must be logged in
    public function beforeRoute( $f3 ) {
        if( is_null( $f3->get( 'SESSION.teacher_email' ) ) && 
            is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@teacher_login' );
        }
        if ( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            Admins::check2FAConfigured( $f3 );
        }
    }

    /**
     * @markets
     * 
     * List all target markets
     */
    public function list( $f3 ) {
        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }
        if( ! is_null( $f3->get( 'SESSION.success' ) ) ) {
            $f3->set( 'success', $f3->get( 'SESSION.success' ) );
            $f3->clear( 'SESSION.success' );
        }
        if( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->set( 'is_admin', true );
        }

        $page = is_null( $f3->get( 'GET.page' ) ) ? 0 : intval( $f3->get( 'GET.page' ) );

        $market = new Market();
        $markets = $market->select(
            'id,name',
            null,
            array( 'order' => 'id ASC' )
        );

        $f3->set( 'markets', $markets );
        $f3->set( 'title', 'Target Markets' );
        
        echo \Template::instance()->render( 'teachers/markets.htm' );
    }    

    /**
     * @edit_market
     * 
     * Edit target market
     */
    public function edit( $f3, $params ) {
        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }
        if( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->set( 'is_admin', true );
        }

        if( ! isset( $params[ 'id' ] ) ) {
            $f3->reroute( '@markets' );
        }
        $id = $params[ 'id' ];

        $market = new Market();
        $market->load( array( 'id=?', $id ) );
        if( $market->dry() ) {
            $f3->reroute( '@markets' );
        }

        $bucket  = $f3->get( 'aws_bucket' );
        $cdn_url = $f3->get( 'aws_url' );
        $asset         = new Asset();
        $market_assets = $asset->get_assets_for_market( $id, $bucket, $cdn_url );
        $asset_types   = Asset::get_asset_types(); 

        // $f3->set( 'asset_base_url', Market::get_base_url( $bucket, $cdn_url ) );
        // $f3->set( 'asset_prefix', Amazon_Storage_Handler::MARKET_KEY_PREFIX );
        // $f3->set( 'asset_suffix', Amazon_Storage_Handler::MARKET_KEY_SUFFIX );

        $f3->set( 'market', $market );
        $f3->set( 'assets', $market_assets );
        $f3->set( 'types', $asset_types );
        $f3->set( 'title', 'Edit target market' );

        echo \Template::instance()->render( 'teachers/market.htm' );
    }

    /**
     * @update_market
     * 
     * Update target market
     */
    public function update( $f3, $params ) {
        if( ! isset( $params[ 'id' ] ) ) {
            $f3->reroute( '@markets' );
        }
        $id = $params[ 'id' ];

        $market = new Market();
        $market->load( array( 'id=?', $id ) );
        if( $market->dry() ) {
            $f3->reroute( '@markets' );
        }

        try {
            $validator = new Validator();
            $post = $f3->get( 'POST' );
            list( $name ) = $validator->getValues( array( 'name' ), $post );
            $validator->checkEmpty( array( $name ) );
            
            $asset = new Asset();
            $types = Asset::get_asset_types();
            foreach( $types as $i => $type ) {
                $type_index = $i + 1;
                $key = 'type_' . $type_index;
                if( isset( $post[ $key ] ) ) {
                    $asset_id = $post[ $key ];
                    $validator->checkNumeric( array( $asset_id ) );
                    
                    $asset->load( array( 'id=? AND delete_soon=0', $asset_id ) );
                    if( $asset->dry() ) {
                        throw new Exception( 'Invalid asset for market' );
                    }
                    $extension = pathinfo( $asset->filename, PATHINFO_EXTENSION );

                    $asset->set_asset_for_market( $id, $type_index, $asset_id );

                    // Update market asset in S3
                    $region = $f3->get( 'aws_region' );
                    $bucket = $f3->get( 'aws_bucket' );
                    $s3_handler = new Amazon_Storage_Handler( $region, $bucket );
                    $source_key = $s3_handler->get_asset_key_name( $asset_id );
                    $dest_key   = $s3_handler->get_market_key_name( $id, $type_index, $extension );
                    $s3_handler->copy_object( $source_key, $dest_key );
                }
            }

            if( isset( $post[ 'scene_id' ] ) && ! empty( $post[ 'scene_id' ] ) ) {
                $market->scene_id = $post[ 'scene_id' ];
            }
            $market->name   = $name;
            $market->save();

            $f3->set( 'SESSION.success', 'Updated target market!' );
            $f3->reroute( '@markets' );
        } catch( Exception $e ) {
            $f3->set( 'SESSION.error', $e->getMessage() );
            $f3->reroute( "@edit_market(@id=$id)" );
        }
    }

}