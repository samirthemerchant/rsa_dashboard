<?php

class Manuals {
    
    const RESULTS_PER_PAGE = 20;

    /**
     * @manuals
     * 
     * List all manuals
     */
    public function list( $f3 ) {
        // User must be logged in
        if( is_null( $f3->get( 'SESSION.teacher_email' ) ) && 
            is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@teacher_login' );
        }
        if ( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            Admins::check2FAConfigured( $f3 );
        }
        
        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }
        if( ! is_null( $f3->get( 'SESSION.success' ) ) ) {
            $f3->set( 'success', $f3->get( 'SESSION.success' ) );
            $f3->clear( 'SESSION.success' );
        }
        if( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->set( 'is_admin', true );
        }

        $page = is_null( $f3->get( 'GET.page' ) ) ? 0 : intval( $f3->get( 'GET.page' ) );

        $manual = new Manual();
        $manuals = $manual->select(
            'id,name,filename,s3_key',
            'delete_soon=0',
            array(
                'order' => 'name ASC',
                'limit' => self::RESULTS_PER_PAGE,
                'offset' => $page * self::RESULTS_PER_PAGE,
            )
        );
        $more_pages_exist = $manual->more_pages_exist( $page, self::RESULTS_PER_PAGE );
        $bucket  = $f3->get( 'aws_bucket' );
        $cdn_url = $f3->get( 'aws_url' );
        foreach( $manuals as $i => $m ) {
            $manuals[ $i ][ 'src_url' ] = Asset::get_src_url( $bucket, $m, $cdn_url );
            $manuals[ $i ][ 'thumbnail' ] = Asset::get_thumbnail( $bucket, $m, $cdn_url );
        }

        $f3->set( 'manuals', $manuals );
        $f3->set( 'page', $page );
        $f3->set( 'results_per_page', self::RESULTS_PER_PAGE );
        $f3->set( 'more_pages_exist', $more_pages_exist );
        $f3->set( 'title', 'Training Manuals' );

        echo \Template::instance()->render( 'teachers/manuals.htm' );  
    }

    /**
     * @new_manual / @edit_manual
     * 
     * New / edit manual
     */
    public function form( $f3, $params ) {
        // Admin only
        if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@teacher_login' );
        }
        Admins::check2FAConfigured( $f3 );

        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }

        // Edit
        if( isset( $params[ 'id' ] ) ) {
            $id = $params[ 'id' ];
            if( empty( $id ) ) {
                $f3->reroute( '@manuals' );
            }
    
            $manual = new Manual();
            $manual->load( array( 'id=? AND delete_soon=0', $id ) );
            if( $manual->dry() ) {
                $f3->reroute( '@manuals' );
            }
            $bucket  = $f3->get( 'aws_bucket' );
            $cdn_url = $f3->get( 'aws_url' );
            $manual->src_url = Asset::get_src_url( $bucket, $manual, $cdn_url );
            $manual->thumbnail = Asset::get_thumbnail( $bucket, $manual, $cdn_url );

            $f3->set( 'manual', $manual );
            $f3->set( 'title', 'Edit Training Manual' );

        // New
        } else {
            $f3->set( 'title', 'New Training Manual' );
        }

        echo \Template::instance()->render( 'admins/manual.htm' );
    }

    /**
     * @create_manual / @update_manual
     * 
     * Create / update manual
     */
    public function save( $f3, $params ) {
        // Admin only
        if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@teacher_login' );
        }
        Admins::check2FAConfigured( $f3 );

        try {
            $validator = new Validator();
            list( $name ) = $validator->getValues( 
                array( 'name' ), 
                $f3->get( 'POST' ) 
            );
            list( $file ) = $validator->getValues(
                array( 'file' ),
                $f3->get( 'FILES' )
            );

            $validator->checkEmpty( array( $name ) );

            $manual = new Manual();

            // Update
            if( isset( $params[ 'id' ] ) ) {
                $id = $params[ 'id' ];
                if( empty( $id ) ) {
                    $f3->reroute( '@manuals' );
                }
                    
                $manual->load( array( 'id=? AND delete_soon=0', $id ) );
                if( $manual->dry() ) {
                    throw new Exception( 'Invalid manual ID' );
                }

                // If file uploaded
                if( isset( $file[ 'error' ] ) ) {
                    if( $file[ 'error' ] == UPLOAD_ERR_OK ) {
                        $validator->checkUnsafeFileType( array( $file ) );
                        // if( ! Clam::scan( $file[ 'tmp_name' ] ) ) {
                        //     throw new Exception( 'Cannot upload unsafe file' );
                        // }        
                        
                        $region = $f3->get( 'aws_region' );
                        $bucket = $f3->get( 'aws_bucket' );
                        $s3_handler   = new Amazon_Storage_Handler( $region, $bucket );
                        $key_name     = $s3_handler->get_manual_key_name( $id );
                        $s3_handler->upload_object( $key_name, $file[ 'tmp_name' ] );
                        
                        $manual->filename = $file[ 'name' ];
                        $manual->s3_key = $key_name;
                    }
                }
                
                $manual->name = $name;
                $manual->date_modified = date( 'Y-m-d H:i:s' );
                $manual->save();    
                $message = 'Updated manual!';
                
                // Create
            } else {
                // Must upload a file
                $validator->checkFileErrors( array( $file ) );
                $validator->checkUnsafeFileType( array( $file ) );
                // if( ! Clam::scan( $file[ 'tmp_name' ] ) ) {
                //     throw new Exception( 'Cannot upload unsafe file' );
                // }

                // We need the ID before uploading to S3.
                $manual->name = $name;
                $manual->save();

                $region = $f3->get( 'aws_region' );
                $bucket = $f3->get( 'aws_bucket' );
                $s3_handler   = new Amazon_Storage_Handler( $region, $bucket );
                $key_name     = $s3_handler->get_manual_key_name( $manual->id );
                $s3_handler->upload_object( $key_name, $file[ 'tmp_name' ] );


                $manual->s3_key   = $key_name;
                $manual->filename = $file[ 'name' ];
                $manual->date_modified = date( 'Y-m-d H:i:s' );
                $manual->update();

                $message = 'Created new manual!';
            }

            $f3->set( 'SESSION.success', $message );

            $f3->reroute( '@manuals' );
        } catch( Exception $e ) {
            $f3->set( 'SESSION.error', $e->getMessage() );
            

            if( is_null( $f3->get( 'POST.id' ) ) ) {
                $f3->reroute( '@new_manual' );
            } else {
                $id = $f3->get( 'POST.id' );
                $f3->reroute( "@edit_manual(@id=$id)" );
            }
        }

    }

    /**
     * @delete_manual
     * 
     * Delete manual
     */
    public function delete( $f3, $params ) {
        header( 'Content-type: application/json' );

        try {
            // Admin only
            if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
                throw new Exception( 'You are not allowed to delete this manual!' );
            }
            Admins::check2FAConfigured( $f3 );
    
            if( isset( $params[ 'id' ] ) ) {
                $id = $params[ 'id' ];
                
                $manual = new Manual();
                $manual->load( array( 'id=? AND delete_soon=0', $id ) );
                if( $manual->dry() ) {
                    throw new Exception( 'Could not find manual!' );
                }
    
                $manual->delete_soon = 1;
                $manual->date_modified = date( 'Y-m-d H:i:s' );
                $manual->save();
                
                echo json_encode( array(
                    'status'  => true,
                    'message' => 'Manual deleted!',
                ) );
            } else {
                throw new Exception( 'Missing manual `id`' );
            }
        } catch( Exception $e ) {
            echo json_encode( array(
                'status'  => false,
                'message' => $e->getMessage(),
            ) );
        }
    }

    public function hard_delete( $f3 ) {
        $logger = new Logger( __DIR__ . '/../scripts/cron.log' );
        $logger->message( 'CRON', null, 'Starting cron task: hard delete manuals...' );

        $max_age = $f3->get( 'SOFT_DELETE_MAX_AGE' );
        $date = date( 'Y-m-d H:i:s', strtotime( "-$max_age days" ) );

        $manual = new Manual();
        $manuals = $manual->select(
            'id',
            array( 'delete_soon=1 AND date_modified<?', $date )
        );
        $count = count( $manuals );
        foreach( $manuals as $row ) {
            $old_manual = new Manual();
            $old_manual->load( array( 'id=?', $row[ 'id' ] ) );

            try {
                $region = $f3->get( 'aws_region' );
                $bucket = $f3->get( 'aws_bucket' );
                $s3_handler = new Amazon_Storage_Handler( $region, $bucket );
                $s3_handler->delete_object( $old_manual->s3_key );
            } catch( Exception $e ) {
                $logger->error( 'Manual', $row[ 'id' ], $e->getMessage() );
            }

            $old_manual->erase();
        }
        $logger->message( 'CRON', null, 'Ending cron task: hard delete manuals.');
        $logger->message( 'CRON', null, "$count manuals permanently deleted." );
    }

}