<?php

class Teachers {

    const RESULTS_PER_PAGE = 20;

    /**
     * @event_lobby
     * 
     * Hubs limbo: redirect to Hubs if event has started, 
     * else shows waiting/ended status
     */
    public function lobby( $f3, $params ) {
        $id = $params[ 'id' ];
        $event = new Event;
        $event->load( array( 'id=? AND delete_soon=0', $id ) );
        if( $event->dry() ) {
            $f3->reroute( $f3->get( 'reticulum_host' ) );
        }
        
        if( is_null( $event->url ) && is_null( $event->hub_id ) ) {
            $f3->set( 'message', 'The 3D CORE booking is not ready. Please come back later.' );
        }
        else if( is_null( $event->hub_id ) ) {
            $f3->set( 'message', 'The 3D CORE booking has ended.' );
        }
        else {
            $asset = new Asset();
            $asset_types_with_video = $asset->get_videos_for_market( $event->market_id );
            $video_param = implode( ',', $asset_types_with_video );

            $event_url_parts = explode( '&v=', $event->url );
            if( $event_url_parts[ 1 ] !== $video_param ) {
                $event->url = $event_url_parts[ 0 ] . '&v=' . $video_param;
                $event->save();
            }
            $f3->reroute( $event->url );
        }

        $f3->set( 'title', 'Booking' );
        echo \Template::instance()->render( 'lobby.htm' );
    }


    /**
     * @teacher_login
     * 
     * Teacher dashboard login page
     */
    public function login( $f3 ) {
        if( ! is_null( $f3->get( 'SESSION.teacher_email' ) ) || 
            ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@teacher_home' );
        }
        if ( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            Admins::check2FAConfigured( $f3 );
        }
        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }

        $session = $f3->get( 'session' );
        $f3->set( 'CSRF', $session->csrf() );
        $f3->copy( 'CSRF', 'SESSION.csrf' );

        $f3->set( 'users', 'Teachers' );
        $f3->set( 'route', 'teacher_check_login' );
        $f3->set( 'title', 'Presenters' );
        echo \Template::instance()->render( 'login.htm' );
    }
    /**
     * @teacher_check_login
     * 
     * Check teacher login credentials
     */
    public function check_login( $f3 ) {
        if( ! is_null( $f3->get( 'SESSION.teacher_email' ) ) || 
            ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@teacher_home' );
        }
        if ( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            Admins::check2FAConfigured( $f3 );
        }

        try {
            $token = $f3->get( 'POST.token' );
            $csrf = $f3->get( 'SESSION.csrf' );
            if ( empty($token) || empty($csrf) || $token !== $csrf ) {
                // CSRF attack detected
                $f3->error( 403 );
            }

            $validator = new Validator();
            list( $email, $password ) = $validator->getValues( 
                array( 'email', 'password' ), 
                $f3->get( 'POST' ) 
            );
            $validator->checkEmail( array( $email ) );
            $validator->checkEmpty( array( $password ) );

            $teacher = new Teacher();
            $teacher->load( array( 'email=? AND delete_soon=0', strtolower( $email ) ) );
            if( $teacher->dry() ) {
                throw new Exception( 'Email or password is incorrect' );
            } else if( ! password_verify( $password, $teacher->password ) ) {
                throw new Exception( 'Email or password is incorrect' );
            }

            // Set cookies
            $f3->set( 'SESSION.teacher_email', $teacher->email );
            $f3->set( 'SESSION.teacher_name', $teacher->name );

            $f3->reroute( '@teacher_home' );
        } catch( Exception $e ) {
            $f3->set( 'SESSION.error', $e->getMessage() );
            $f3->reroute( '@teacher_login' );
        }
    }

    /**
     * @teacher_logout
     * 
     * Logout teacher
     */
    public function logout( $f3 ) {
        if( is_null( $f3->get( 'SESSION.teacher_email' ) ) &&
            is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@teacher_login' );
        }

        $f3->clear( 'SESSION' );
        $f3->reroute( '@teacher_login' );
    }

    /**
     * @teacher_password
     * 
     * Change teacher password form
     */
    public function change_password( $f3 ) {
        if( is_null( $f3->get( 'SESSION.teacher_email' ) ) ) {
            $f3->reroute( '@teacher_login' );
        }
        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }

        $f3->set( 'users', 'Teachers' );
        $f3->set( 'route', 'teacher_update_password' );
        $f3->set( 'title', 'Change Password' );
        echo \Template::instance()->render( 'password.htm' );   
    }
    /**
     * @update_teacher_password
     * 
     * Update teacher password
     */
    public function update_password( $f3 ) {
        if( is_null( $f3->get( 'SESSION.teacher_email' ) ) ) {
            $f3->reroute( '@teacher_login' );
        }

        try {
            $validator = new Validator();
            list( $old_password, $new_password, $confirm_password ) = $validator->getValues( 
                array( 'old_password', 'new_password', 'confirm_password' ),
                $f3->get( 'POST' )
            );
            $validator->checkEmpty( array( $old_password, $new_password, $confirm_password ) );
            // TODO: Check new password length, contains numbers, special chars, etc.

            $teacher = new Teacher();
            $email = $f3->get( 'SESSION.teacher_email' );
            $teacher->load( array( 'email=? AND delete_soon=0', $email ) );
            if( $teacher->dry() ) {
                // Session data is corrupted somehow
                throw new Exception( 'Could not find user. Please login again.' );
            } else if( ! password_verify( $old_password, $teacher->password ) ) {
                throw new Exception( 'Old password is incorrect' );
            } else if( $new_password !== $confirm_password ) {
                throw new Exception( 'New passwords do not match' );
            }

            $teacher->password = password_hash( $new_password, PASSWORD_DEFAULT );
            $teacher->save();
            $f3->set( 'SESSION.success', 'Password updated!' );

            $f3->reroute( '@teacher_home' );
        } catch( Exception $e ) {
            $f3->set( 'SESSION.error', $e->getMessage() );
            $f3->reroute( '@teacher_password' );
        }

    }

    /**
     * @teacher_home
     * 
     * Teacher dashboard main page
     */
    public function home( $f3 ) {
        if( is_null( $f3->get( 'SESSION.teacher_email' ) ) &&
            is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@teacher_login' );
        }
        if ( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            Admins::check2FAConfigured( $f3 );
        }
        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }
        if( ! is_null( $f3->get( 'SESSION.success' ) ) ) {
            $f3->set( 'success', $f3->get( 'SESSION.success' ) );
            $f3->clear( 'SESSION.success' );
        }
        if( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->set( 'is_admin', true );
        }

        $f3->set( 'title', 'Home' );

        if( is_null( $f3->get( 'SESSION.teacher_name' ) ) ) {
            $f3->set( 'name', $f3->get( 'SESSION.admin_name' ) );
        } else {
            $f3->set( 'name', $f3->get( 'SESSION.teacher_name' ) );
        }
        echo \Template::instance()->render( 'teachers/home.htm' );
    }


    /** *** ADMIN ONLY *** */

    /**
     * @teachers
     * 
     * List teachers
     */
    public function list( $f3 ) {
        if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@admin_login' );
        }
        Admins::check2FAConfigured( $f3 );

        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }
        if( ! is_null( $f3->get( 'SESSION.success' ) ) ) {
            $f3->set( 'success', $f3->get( 'SESSION.success' ) );
            $f3->clear( 'SESSION.success' );
        }

        $page = is_null( $f3->get( 'GET.page' ) ) ? 0 : intval( $f3->get( 'GET.page' ) );

        $teacher = new Teacher();
        $teachers = $teacher->select(
            'id,name,email',
            'delete_soon=0',
            array( 
                'order' => 'name ASC, email ASC',
                'limit' => self::RESULTS_PER_PAGE,
                'offset' => $page * self::RESULTS_PER_PAGE
            )
        );
        $more_pages_exist = $teacher->more_pages_exist( $page, self::RESULTS_PER_PAGE );

        $f3->set( 'teachers', $teachers );
        $f3->set( 'page', $page );
        $f3->set( 'results_per_page', self::RESULTS_PER_PAGE );
        $f3->set( 'more_pages_exist', $more_pages_exist );
        $f3->set( 'title', 'Presenters' );

        echo \Template::instance()->render( 'admins/teachers.htm' );
    }

    /**
     * @new_teacher
     * 
     * New teacher form
     */
    public function new( $f3 ) {
        if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@admin_login' );
        }
        Admins::check2FAConfigured( $f3 );
        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }

        $f3->set( 'title', 'New Presenter' );
        echo \Template::instance()->render( 'admins/teacher.htm' );
    }

    /**
     * @create_teacher
     * 
     * Create new teacher
     */
    public function create( $f3 ) {
        if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@admin_login' );
        }
        Admins::check2FAConfigured( $f3 );
        try {
            $validator = new Validator();
            $post = $f3->get( 'POST' );
            list( $name, $email, $account_id ) = $validator->getValues(
                array( 'name', 'email', 'account_id' ),
                $post
            );
            $validator->checkEmpty( array( $name, $email ) );
            $validator->checkEmail( array( $email ) );

            $email = strtolower( $email );
            $teacher = new Teacher();
            if( $teacher->count( array( "email=? AND delete_soon=0", $email ) ) > 0 ) {
                throw new Exception( 'User with same email address already exists!' );
            }
            
            $password               = $teacher->generateRandomPassword();
            $teacher->account_id    = $account_id;
            $teacher->name          = $name;
            $teacher->email         = $email;
            $teacher->password      = password_hash( 
                $password, 
                PASSWORD_DEFAULT 
            );
            $teacher->date_modified = date( 'Y-m-d H:i:s' );

            $email_host = $f3->get( 'emailhost' );
            $email_username = $f3->get( 'emailuser' );
            $email_password = $f3->get( 'emailpass' );

            $login_url      = $f3->get( 'dashboard_base_url' ) . $f3->alias( 'teacher_login' );
            $emailer        = new Emailer( $email_host, $email_username, $email_password, $login_url );
            $emailer->welcome_message( $name, $email, $password );

            $teacher->save();

            $f3->set( 'SESSION.success', 'New presenter invited!' );
            $f3->reroute( '@teachers' );
        } catch( Exception $e ) {
            $f3->set( 'SESSION.error', $e->getMessage() );
            $f3->reroute( '@new_teacher' );
        }
    }

    /**
     * @delete_teacher
     * 
     * Remove teacher account
     */
    public function delete( $f3, $params ) {
        header( 'Content-type: application/json' );

        try {
            if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
                throw new Exception( 'You are not allowed to delete this presenter!' );
            }
            Admins::check2FAConfigured( $f3 );
    
            if( ! isset( $params[ 'id' ] ) ) {
                throw new Exception( 'Missing presenter `id`!' );
            }
            $id = $params[ 'id' ];
    
            $teacher = new Teacher();
            $teacher->load( array( 'id=? AND delete_soon=0', $id ) );
            if( $teacher->dry() ) {
                throw new Exception( 'Could not find presenter!' );
            }
            $teacher->delete_soon = 1;
            $teacher->date_modified = date( 'Y-m-d H:i:s' );
            $teacher->save();
    
            echo json_encode( array(
                'status'  => true,
                'message' => 'Presenter deleted!',
            ) );
        } catch( Exception $e ) {
            echo json_encode( array(
                'status'  => false,
                'message' => $e->getMessage(),
            ) );
        }
    }

    public function hard_delete( $f3 ) {
        $logger = new Logger( __DIR__ . '/../scripts/cron.log' );
        $logger->message( 'CRON', null, 'Starting cron task: hard delete teachers...' );

        $max_age = $f3->get( 'SOFT_DELETE_MAX_AGE' );
        $date = date( 'Y-m-d H:i:s', strtotime( "-$max_age days" ) );

        $teacher = new Teacher();
        $teachers = $teacher->select(
            'id',
            array( 'delete_soon=1 AND date_modified<?', $date )
        );
        $count = count( $teachers );
        foreach( $teachers as $row ) {
            $old_teacher = new Teacher();
            $old_teacher->load( array( 'id=?', $row[ 'id' ] ) );
            $old_teacher->erase();
        }
        $logger->message( 'CRON', null, 'Ending cron task: hard delete teachers.');
        $logger->message( 'CRON', null, "$count teachers permanently deleted." );
    }


}