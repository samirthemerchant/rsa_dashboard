<?php

class Events {

    const RESULTS_PER_PAGE = 20,
        MAX_EVENT_DURATION = 72;// hours

    // User must be logged in
    public function beforeRoute( $f3 ) {
        if( is_null( $f3->get( 'SESSION.teacher_email' ) ) && 
            is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@teacher_login' );
        }
        if ( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            Admins::check2FAConfigured( $f3 );
        }
    }

    /**
     * spawn_rooms
     * Scheduled task to spawn rooms for events
     * at scheduled time
     */
    public function spawn_rooms( $email_host, $email_username, $email_password, 
                                $reticulum_host, $api_host, $api_port, $api_secret,
                                $static_url, $share_url, $base_url ) {
        $logger = new Logger( __DIR__ . '/../scripts/cron.log' );
        $logger->message( 'CRON', null, 'Starting cron task: `spawn_rooms`...');

        $event = new Event();
        $hub = new Hub_Generator( $reticulum_host, $api_host, $api_port, $api_secret );

        $events = $event->get_started_events();

        $successful_tasks = 0;
        $errors = 0;
        foreach( $events as $started_event ) {
            try {
                $response = $hub->new( $started_event[ 'title' ], $started_event[ 'scene_id' ] );
                if( isset( $response[ 'hub_id'] ) ) {
                    $hub_id = $response[ 'hub_id' ];

                    $asset = new Asset();
                    $asset_types_with_video = $asset->get_videos_for_market( $started_event[ 'market_id' ] );

                    $event->load( array( 'id=?', $started_event[ 'id' ] ) );
                    $event->url    = $response[ 'url' ] . '/?market=' . $started_event[ 'market_id' ] . '&v=' . implode( ',', $asset_types_with_video );
                    $event->hub_id = $hub_id;
                    $event->save();
                    $message = "Created new room with Hub ID `$hub_id`.";
                    $logger->message( 'Event', $started_event[ 'id' ], $message );
                    
                    // Set ownership and settings
                    list( $status_code, $response ) = $hub->amend_room( $hub_id, $started_event[ 'account_id' ] );
                    if( '200' != $status_code ) {
                        throw new Exception( "Polycosm API bad response ($status_code - $response)" );
                    }
                    $message = "Fixed settings for room with Hub ID `$hub_id`.";
                    $logger->message( 'Event', $started_event[ 'id' ], $message );
    
                    $teacher = $event->get_event_teacher();
                    $event_details = $event->get_event_details( $base_url, $share_url );
                    $emailer = new Emailer( $email_host, $email_username, $email_password, $static_url );
                    $emailer->event_room_details( $teacher, $event_details );
    
                    $message = "Sent booking confirmation to presenter.";
                    $logger->message( 'Event', $started_event[ 'id' ], $message );

                    $fields = array(
                        'email' => $teacher->email,
                        'target_market' => intval( $started_event[ 'market_id' ] )
                    );
                    Google_Analytics::track_event( 'room', 'opened', null, $fields );
    
                    $event->reset();
                    $successful_tasks++;
                } else {
                    $logger->error( 'Event', $started_event[ 'id' ], 'Failed to open room for event!');
                    $errors++;
                }
            } catch( Exception $e ) {
                $logger->error( 'Event', $started_event[ 'id' ], $e->getMessage() );
                $errors++;
            }
        }

        $logger->message( 'CRON', null, 'Ending cron task: `spawn_rooms`.');
        $logger->message( 'CRON', null, "$successful_tasks events processed successfully.");
        $logger->message( 'CRON', null, "$errors errors occurred.");
    }

    /**
     * close_rooms
     * Scheduled task to close rooms for events
     * that have ended.
     */
    public function close_rooms( $reticulum_host, $admin_header_name, $admin_header_value ) {
        $logger = new Logger( __DIR__ . '/../scripts/cron.log' );
        $logger->message( 'CRON', null, 'Starting cron task: `close_rooms`...');

        $event = new Event();
        $hub = new Hub_Generator( $reticulum_host );
        $hub->set_admin_header( $admin_header_name, $admin_header_value );

        $now = date( 'Y-m-d H:i:s' );
        $events = $event->select(
            'id, hub_id, market_id',
            array( 'end_date < ? AND hub_id IS NOT NULL', $now ),
            array( 'order' => 'end_date ASC' )
        );

        $successful_tasks = 0;
        $errors = 0;
        foreach( $events as $ended_event ) {
            try {
                $status_code = $hub->close( $ended_event[ 'hub_id' ] );
                if( '200' == $status_code ) {
                    $event->load( array( 'id=?', $ended_event[ 'id' ] ) );
                    $event->hub_id = null;
                    $event->save();
                    $event->reset();

                    $fields = array(
                        'target_market' => intval( $event->market_id )
                    );
                    Google_Analytics::track_event( 'room', 'closed', null, $fields );
                    
                    $logger->message( 'Event', $ended_event[ 'id' ], 'Closed room for event.' );
                    $successful_tasks++;
                } else {
                    $logger->error( 'Event', $ended_event[ 'id' ], "HTTP request responded with status $status_code: could not close room for event!" );
                    $errors++;
                }

            } catch( Exception $e ) {
                $logger->error( 'Event', $ended_event[ 'id' ], $e->getMessage() );
                $errors++;
            }
        }

        $logger->message( 'CRON', null, 'Ending cron task: `close_rooms`.');
        $logger->message( 'CRON', null, "$successful_tasks events processed successfully.");
        $logger->message( 'CRON', null, "$errors errors occurred.");
    }

    
    /**
     * @events
     * 
     * List all events
     */
    public function list( $f3 ) {
        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }
        if( ! is_null( $f3->get( 'SESSION.success' ) ) ) {
            $f3->set( 'success', $f3->get( 'SESSION.success' ) );
            $f3->clear( 'SESSION.success' );
        }
        if( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->set( 'is_admin', true );
        }

        $page = is_null( $f3->get( 'GET.page' ) ) ? 0 : intval( $f3->get( 'GET.page' ) );
        $filter = null;

        if( ! $f3->get( 'is_admin' ) ) {
            $teacher = new Teacher();
            $teacher->load( array( 'email=? AND delete_soon=0', $f3->get( 'SESSION.teacher_email') ) );
            $filter = array( 'teacher_id=?', $teacher->id );
        }

        $event = new Event();
        $events = $event->get_event_list_details( 
            $filter, 
            self::RESULTS_PER_PAGE, 
            $page * self::RESULTS_PER_PAGE 
        );
        $more_pages_exist = $event->more_pages_exist( $page, self::RESULTS_PER_PAGE, $filter );

        $events = array_map( array( $this, 'format_date' ), $events );

        $f3->set( 'events', $events );
        $f3->set( 'page', $page );
        $f3->set( 'results_per_page', self::RESULTS_PER_PAGE );
        $f3->set( 'more_pages_exist', $more_pages_exist);
        $f3->set( 'title', 'Events' );

        echo \Template::instance()->render( 'teachers/events.htm' );  
    }

    /**
     * @new_event / @edit_event
     * 
     * New / Edit event
     */
    public function form( $f3, $params ) {
        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }
        if( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->set( 'is_admin', true );
        }

        // Edit
        if( isset( $params[ 'id' ] ) ) {
            $id = $params[ 'id' ];
            if( empty( $id ) ) {
                $f3->reroute( '@events' );
            }
    
            $event = new Event();
            $event->load( array( 'id=? AND delete_soon=0', $id ) );
            if( $event->dry() ) {
                $f3->reroute( '@events' );
            }

            if( ! $f3->get( 'is_admin' ) ) {
                $teacher = new Teacher();
                $teacher->load( array( 'email=? AND delete_soon=0', $f3->get( 'SESSION.teacher_email') ) );
                if( $event->teacher_id !== $teacher->id ) {
                    $f3->reroute( '@events' );
                }
            }
            
            $event->short_start_date = date( 'Y-m-d', strtotime( $event->start_date ) );
            $event->start_time = date( 'H:i:s', strtotime( $event->start_date ) );
            $event->short_end_date = date( 'Y-m-d', strtotime( $event->end_date ) );
            $event->end_time = date( 'H:i:s', strtotime( $event->end_date ) );
            
            $f3->set( 'event', $event );
            $f3->set( 'route', 'update_event' );
            $f3->set( 'title', 'Edit event' );
            
        // New
        } else {
            $f3->set( 'route', 'create_event' );
            $f3->set( 'title', 'New event' );
        }

        $market = new Market();
        $markets = $market->select( 
            'id,name', 
            null, 
            array( 'order' => 'id ASC' ) 
        );
        if( $event ) {
            array_map( array( $event, 'is_market_selected' ), $markets );
        }
        $f3->set( 'markets', $markets );

        $teacher = new Teacher();
        $teachers = $teacher->select(
            'id,name,email',
            'delete_soon=0',
            array( 'order' => 'name ASC' )
        );
        if( $event ) {
            array_map( array( $event, 'is_teacher_selected' ), $teachers );
        }
        $f3->set( 'teachers', $teachers );
        $f3->set( 'locations', Event::get_locations() );

        echo \Template::instance()->render( 'teachers/event.htm' );
    }

    /**
     * @create_event / @update_event
     * 
     * Create / update event
     */
    public function save( $f3, $params ) {
        if( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->set( 'is_admin', true );
        }

        try {
            $validator = new Validator();
            $post = $f3->get( 'POST' );
            list( $title, $market_id, $location, $students, $start_date, $start_time, $end_date, $end_time, $teacher_id ) = $validator->getValues( 
                array( 'title', 'market_id', 'location', 'students', 'start_date', 'start_time', 'end_date', 'end_time', 'teacher_id' ), 
                $post
            );
            $validator->checkEmpty( array( $title, $location, $start_date, $start_time, $end_date, $end_time ) );
            $validator->checkNumeric( array( $market_id, $teacher_id, $students ) );

            $start_date_time = strtotime( "$start_date $start_time" );
            $end_date_time   = strtotime( "$end_date $end_time" );
            if( $start_date_time < time() ) {
                throw new Exception( 'Event start date/time must be in the future' );
            }
            if( $start_date_time > $end_date_time ) {
                throw new Exception( 'Event end date/time must be after start date/time');
            }
            $max_event_duration = self::MAX_EVENT_DURATION;
            if( $end_date_time - $start_date_time > ( $max_event_duration * 60 * 60 ) ) {
                throw new Exception( "Maximum event duration is $max_event_duration hours" );
            }
            if( ! in_array( $location, Event::get_locations() ) ) {
                throw new Exception( 'Invalid location' );
            }
            $students = intval( $students );
            if( 1 > $students || 100 < $students ) {
                throw new Exception( 'Number of students must be value in range of 1-100.');
            }

            $now = date( 'Y-m-d H:i:s' );
            $event = new Event();
            
            // Update
            if( isset( $params[ 'id' ] ) ) {
                $id = $params[ 'id' ];
                
                $event->load( array( 'id=? AND delete_soon=0', $id ) );
                if( $event->dry() ) {
                    throw new Exception( 'Invalid event ID' );
                }

                if( ! $f3->get( 'is_admin' ) ) {
                    $teacher = new Teacher();
                    $teacher->load( array( 'email=? AND delete_soon=0', $f3->get( 'SESSION.teacher_email') ) );
                    if( $event->teacher_id !== $teacher->id ) {
                        $f3->reroute( '@events' );
                    }
                }    

                $message = 'Updated event!';
                
            // Create
            } else {
                $event->date_created = $now;
                $message = 'Created new event!';
            }

            $event->title         = $title;
            $event->market_id     = intval( $market_id );
            $event->start_date    = date( 'Y-m-d H:i:s', $start_date_time );
            $event->end_date      = date( 'Y-m-d H:i:s', $end_date_time );
            $event->teacher_id    = $teacher_id;
            $event->date_modified = $now;
            $event->use_temp_urls = isset( $post[ 'use_temp_urls' ] ) ? 1 : 0;
            $event->location      = $location;
            $event->students      = $students;
            $event->save();

            // Send booking confirmation
            $email_host = $f3->get( 'emailhost' );
            $email_username = $f3->get( 'emailuser' );
            $email_password = $f3->get( 'emailpass' );
            $manuals_url    = $f3->get( 'dashboard_base_url' ) . $f3->alias( 'manuals' );
            $teacher = $event->get_event_teacher();
            $event_details = $event->get_event_details( $f3->get( 'dashboard_base_url' ) );
            $emailer = new Emailer( $email_host, $email_username, $email_password, $manuals_url );
            $emailer->booking_confirmation( $teacher, $event_details );

            $fields = array(
                'email'         => $teacher->email,
                'target_market' => intval( $market_id ),
                'location'      => $location,
                'students'      => $students
            );
            Google_Analytics::track_event( 'room', 'created', null, $fields );
            Google_Analytics::track_event( 'county', 'selected', $location, null );

            $f3->set( 'SESSION.success', $message );
            $f3->reroute( '@events' );
        } catch( Exception $e ) {
            $f3->set( 'SESSION.error', $e->getMessage() );

            if( isset( $params[ 'id' ] ) ) {
                $id = $params[ 'id' ];
                $f3->reroute( "@edit_event(@id=$id)" );
            } else {
                $f3->reroute( '@new_event' );
            }
        }

    }

    /**
     * @delete_event
     * 
     * Delete event
     */
    public function delete( $f3, $params ) {
        header( 'Content-type: application/json' );
        
        if( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->set( 'is_admin', true );
        }

        try {
            if( isset( $params[ 'id' ] ) ) {
                $id = $params[ 'id' ];
                
                $event = new Event();
                $event->load( array( 'id=? AND delete_soon=0', $id ) );
                if( $event->dry() ) {
                    throw new Exception( 'Could not find event!' );
                }
    
                if( ! $f3->get( 'is_admin' ) ) {
                    $teacher = new Teacher();
                    $teacher->load( array( 'email=? AND delete_soon=0', $f3->get( 'SESSION.teacher_email') ) );
                    if( $event->teacher_id !== $teacher->id ) {
                        throw new Exception( 'You are not allowed to delete this event!' );
                    }
                }
    
                $event->delete_soon = 1;
                $event->date_modified = date( 'Y-m-d H:i:s' );
                $event->save();

                echo json_encode( array( 
                    'status'  => true,
                    'message' => 'Event deleted!',
                ) );
            } else {
                throw new Exception( 'Missing event `id`!' );
            }
        } catch ( Exception $e ) {
            echo json_encode( array(
                'status'  => false,
                'message' => $e->getMessage(),
            ) );
        }

    }

    public function hard_delete( $f3 ) {
        $logger = new Logger( __DIR__ . '/../scripts/cron.log' );
        $logger->message( 'CRON', null, 'Starting cron task: hard delete events...' );

        $max_age = $f3->get( 'SOFT_DELETE_MAX_AGE' );
        $date = date( 'Y-m-d H:i:s', strtotime( "-$max_age days" ) );

        $event = new Event();
        $events = $event->select(
            'id',
            array( 'delete_soon=1 AND date_modified<?', $date )
        );
        $count = count( $events );
        foreach( $events as $row ) {
            $old_event = new Event();
            $old_event->load( array( 'id=?', $row[ 'id' ] ) );
            $old_event->erase();
        }
        $logger->message( 'CRON', null, 'Ending cron task: hard delete events.');
        $logger->message( 'CRON', null, "$count events permanently deleted." );
    }


    /**
     * Utility function (callable for array functions)
     * Format event date field
     */
    private function format_date( $event ) {
        $event[ 'start_date' ] = date( 'D jS M g:i a', strtotime( $event[ 'start_date' ] ) );
        $event[ 'end_date' ]   = date( 'D jS M g:i a', strtotime( $event[ 'end_date' ] ) );
        return $event;
    }
}