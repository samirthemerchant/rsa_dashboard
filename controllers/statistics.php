<?php

class Statistics {

    // User must be admin
    public function beforeRoute( $f3 ) {
        if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@admin_login' );
        }
        if ( ! is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            Admins::check2FAConfigured( $f3 );
        }
    }

    /**
     * @statistics
     * 
     * List all statistics
     */
    public function list( $f3 ) {
        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }
        if( ! is_null( $f3->get( 'SESSION.success' ) ) ) {
            $f3->set( 'success', $f3->get( 'SESSION.success' ) );
            $f3->clear( 'SESSION.success' );
        }

        $statistic = new Statistic();
        $statistics = $statistic->select(
            'id,text,value',
            null,
            array( 'order' => 'id ASC' )
        );

        $f3->set( 'statistics', $statistics );
        $f3->set( 'title', 'Statistics' );
        
        echo \Template::instance()->render( 'admins/statistics.htm' );
    }    

    /**
     * @edit_statistic
     * 
     * Edit statistic
     */
    public function edit( $f3, $params ) {
        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }

        if( ! isset( $params[ 'id' ] ) ) {
            $f3->reroute( '@statistics' );
        }
        $id = $params[ 'id' ];

        $statistic = new Statistic();
        $statistic->load( array( 'id=?', $id ) );
        if( $statistic->dry() ) {
            $f3->reroute( '@statistics' );
        }
        $bucket  = $f3->get( 'aws_bucket' );
        $cdn_url = $f3->get( 'aws_url' );
        $statistic->set_src_url( $bucket, $cdn_url );

        $f3->set( 'statistic', $statistic );
        $f3->set( 'nonce', bin2hex( random_bytes( 7 ) ) );
        $f3->set( 'title', 'Edit statistic' );

        echo \Template::instance()->render( 'admins/statistic.htm' );
    }

    /**
     * @update_statistic
     * 
     * Update statistic
     */
    public function update( $f3, $params ) {
        if( ! isset( $params[ 'id' ] ) ) {
            $f3->reroute( '@statistics' );
        }
        $id = $params[ 'id' ];

        $statistic = new Statistic();
        $statistic->load( array( 'id=?', $id ) );
        if( $statistic->dry() ) {
            $f3->reroute( '@statistics' );
        }

        try {
            $validator = new Validator();
            list( $value ) = $validator->getValues( 
                array( 'value' ), 
                $f3->get( 'POST' ) 
            );
            $validator->checkEmpty( array( $value ) );
            
            
            $font_path = __DIR__ . '/../util/font.ttf';
            if( ! file_exists( $font_path ) ) {
                throw new Exception( 'Could not locate font ' . $font_path );
            }
            $image_generator = new Image_Generator( $font_path );
            $temp_path = $image_generator->draw( $value );
            
            $region = $f3->get( 'aws_region' );
            $bucket = $f3->get( 'aws_bucket' );
            $s3_handler = new Amazon_Storage_Handler( $region, $bucket );
            $key_name   = $s3_handler->get_statistic_key_name( $id );
            $s3_handler->upload_object( $key_name, $temp_path );

            $image_generator->clean();
            
            $statistic->value = $value;
            $statistic->s3_key = $key_name;
            $statistic->save();

            $f3->set( 'SESSION.success', 'Updated statistic!' );
            $f3->reroute( '@statistics' );
        } catch( Exception $e ) {
            $f3->set( 'SESSION.error', $e->getMessage() );
            $f3->reroute( '@edit_statistic' );
        }
    }

}