<?php

class Assets {
    
    const RESULTS_PER_PAGE = 20;

    // Admin must be logged in
    public function beforeRoute( $f3 ) {
        if( is_null( $f3->get( 'SESSION.admin_email' ) ) ) {
            $f3->reroute( '@admin_login' );
        }
        Admins::check2FAConfigured( $f3 );
    }

    /**
     * @assets
     * 
     * List all assets
     */
    public function list( $f3 ) {
        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }
        if( ! is_null( $f3->get( 'SESSION.success' ) ) ) {
            $f3->set( 'success', $f3->get( 'SESSION.success' ) );
            $f3->clear( 'SESSION.success' );
        }

        $page = is_null( $f3->get( 'GET.page' ) ) ? 0 : intval( $f3->get( 'GET.page' ) );

        $asset = new Asset();
        $assets = $asset->select(
            'id,name,filename',
            'delete_soon=0',
            array(
                'order' => 'name ASC',
                'limit' => self::RESULTS_PER_PAGE,
                'offset' => $page * self::RESULTS_PER_PAGE,
            )
        );
        $more_pages_exist = $asset->more_pages_exist( $page, self::RESULTS_PER_PAGE );

        $f3->set( 'assets', $assets );
        $f3->set( 'page', $page );
        $f3->set( 'results_per_page', self::RESULTS_PER_PAGE );
        $f3->set( 'more_pages_exist', $more_pages_exist );
        $f3->set( 'title', 'Assets' );

        echo \Template::instance()->render( 'admins/assets.htm' );  
    }

    /**
     * @new_asset / @edit_asset
     * 
     * New / edit asset
     */
    public function form( $f3, $params ) {
        if( ! is_null( $f3->get( 'SESSION.error' ) ) ) {
            $f3->set( 'error', $f3->get( 'SESSION.error' ) );
            $f3->clear( 'SESSION.error' );
        }

        $market = new Market();
        $markets = $market->select( 
            'id,name,0 AS selected', 
            null, 
            array( 'order' => 'id ASC' ) 
        );
        $types = Asset::get_asset_types();

        // Edit
        if( isset( $params[ 'id' ] ) ) {
            $id = $params[ 'id' ];
            if( empty( $id ) ) {
                $f3->reroute( '@assets' );
            }
    
            $asset = new Asset();
            $asset->load( array( 'id=? AND delete_soon=0', $id ) );
            if( $asset->dry() ) {
                $f3->reroute( '@assets' );
            }
            if( 1 == $asset->delete_soon ) {
                $f3->reroute( '@assets' );
            }
            $bucket  = $f3->get( 'aws_bucket' );
            $cdn_url = $f3->get( 'aws_url' );
            $asset->src_url = Asset::get_src_url( $bucket, $asset, $cdn_url );
            $asset->thumbnail = Asset::get_thumbnail( $bucket, $asset, $cdn_url );

            list( $market_ids, $asset_types ) = $asset->get_asset_settings();
            foreach( $markets as $market ) {
                $market->selected = in_array( $market->id, $market_ids );
            }
            $selected_types = array();
            foreach( $asset_types as $type_index ) {
                $selected_types[ $type_index ] = true;
            }

            $f3->set( 'asset', $asset );
            $f3->set( 'selected', $selected_types );
            $f3->set( 'title', 'Edit Asset' );

        // New
        } else {
            $f3->set( 'title', 'New Asset' );
        }

        $f3->set( 'markets', $markets );
        $f3->set( 'types', Asset::get_asset_types() );
        echo \Template::instance()->render( 'admins/asset.htm' );
    }

    /**
     * @create_asset / @update_asset
     * 
     * Create / update asset
     */
    public function save( $f3, $params ) {
        try {
            $validator = new Validator();
            list( $name, $market_ids, $type ) = $validator->getValues( 
                array( 'name', 'market_ids', 'type' ), 
                $f3->get( 'POST' ) 
            );
            list( $file ) = $validator->getValues(
                array( 'file' ),
                $f3->get( 'FILES' )
            );

            $validator->checkEmpty( array( $name ) );

            $asset = new Asset();

            // Update
            if( isset( $params[ 'id' ] ) ) {
                $id = $params[ 'id' ];
                if( empty( $id ) ) {
                    $f3->reroute( '@assets' );
                }
                    
                $asset->load( array( 'id=? AND delete_soon=0', $id ) );
                if( $asset->dry() ) {
                    throw new Exception( 'Invalid asset ID' );
                }
                if( 1 == $asset->delete_soon ) {
                    $f3->reroute( '@assets' );
                }

                // If file uploaded
                if( isset( $file[ 'error' ] ) ) {
                    if( $file[ 'error' ] == UPLOAD_ERR_OK ) {
                        $validator->checkUnsafeFileType( array( $file ) );
                        // if( ! Clam::scan( $file[ 'tmp_name' ] ) ) {
                        //     throw new Exception( 'Cannot upload unsafe file' );
                        // }        

                        // $temp_path = Uploader::uploadFile( 
                        //     $file, 
                        //     Asset::TEMP_MEDIA_DIR 
                        // );
                        // $asset->temp_path = $temp_path;
                        $region = $f3->get( 'aws_region' );
                        $bucket = $f3->get( 'aws_bucket' );
                        $s3_handler   = new Amazon_Storage_Handler( $region, $bucket );
                        $key_name     = $s3_handler->get_asset_key_name( $id );
                        $s3_handler->upload_object( $key_name, $file[ 'tmp_name' ] );
                        
                        $asset->filename = $file[ 'name' ];
                        $asset->s3_key = $key_name;

                        // Update scenes with asset...
                        $selected_assets = $asset->get_selected_market_assets();
                        foreach( $selected_assets as $selected_asset ) {
                            $dest_key  = $s3_handler->get_market_key_name( 
                                $selected_asset[ 'market_id' ], 
                                $selected_asset[ 'asset_type' ],
                                pathinfo( $asset->filename, PATHINFO_EXTENSION ) 
                            );
                            $s3_handler->copy_object( $key_name, $dest_key );
                        }
                    }
                }

                $asset->name = $name;
                $asset->date_modified = date( 'Y-m-d H:i:s' );
                $asset->save();    
                $message = 'Updated asset!';
                
            // Create
            } else {
                // Must upload a file
                $validator->checkFileErrors( array( $file ) );
                $validator->checkUnsafeFileType( array( $file ) );
                // if( ! Clam::scan( $file[ 'tmp_name' ] ) ) {
                //     throw new Exception( 'Cannot upload unsafe file' );
                // }        

                // We need the ID before uploading to S3.
                $asset->name = $name;
                $asset->date_modified = date( 'Y-m-d H:i:s' );
                $asset->save();

                $region = $f3->get( 'aws_region' );
                $bucket = $f3->get( 'aws_bucket' );
                $s3_handler   = new Amazon_Storage_Handler( $region, $bucket );
                $key_name     = $s3_handler->get_asset_key_name( $asset->id );
                $s3_handler->upload_object( $key_name, $file[ 'tmp_name' ] );


                $asset->s3_key   = $key_name;
                $asset->filename = $file[ 'name' ];
                $asset->update();

                $message = 'Created new asset!';
            }

            $asset->set_asset_settings( $market_ids, $type );
            $f3->set( 'SESSION.success', $message );

            $f3->reroute( '@assets' );
        } catch( Exception $e ) {
            $f3->set( 'SESSION.error', $e->getMessage() );
            

            if( is_null( $f3->get( 'POST.id' ) ) ) {
                $f3->reroute( '@new_asset' );
            } else {
                $id = $f3->get( 'POST.id' );
                $f3->reroute( "@edit_asset(@id=$id)" );
            }
        }

    }

    /**
     * @delete_asset
     * 
     * Delete asset
     */
    public function delete( $f3, $params ) {
        header( 'Content-type: application/json' );

        try {
            if( isset( $params[ 'id' ] ) ) {
                $id = $params[ 'id' ];
                
                $asset = new Asset();
                $asset->load( array( 'id=? AND delete_soon=0', $id ) );
                if( $asset->dry() ) {
                    throw new Exception( 'Could not find asset!' );
                }
                
                $asset->delete_soon = 1;
                $asset->date_modified = date( 'Y-m-d H:i:s' );
                $asset->erase_settings();

                $asset->save();

                echo json_encode( array(
                    'status'  => true,
                    'message' => 'Asset deleted!',
                ) );
            } else {
                throw new Exception( 'Missing asset `id`!');
            }
        } catch( Exception $e ) {
            echo json_encode( array(
                'status'  => false,
                'message' => $e->getMessage(),
            ) );
        }
    }

    public function hard_delete( $f3 ) {
        $logger = new Logger( __DIR__ . '/../scripts/cron.log' );
        $logger->message( 'CRON', null, 'Starting cron task: hard delete assets...' );

        $max_age = $f3->get( 'SOFT_DELETE_MAX_AGE' );
        $date = date( 'Y-m-d H:i:s', strtotime( "-$max_age days" ) );

        $asset = new Asset();
        $assets = $asset->select(
            'id',
            array( 'delete_soon=1 AND date_modified<?', $date )
        );
        $count = count( $assets );
        foreach( $assets as $row ) {
            $old_asset = new Asset();
            $old_asset->load( array( 'id=?', $row[ 'id' ] ) );

            try {
                $region = $f3->get( 'aws_region' );
                $bucket = $f3->get( 'aws_bucket' );
                $s3_handler = new Amazon_Storage_Handler( $region, $bucket );
                $s3_handler->delete_object( $old_asset->s3_key );
            } catch( Exception $e ) {
                $logger->error( 'Asset', $row[ 'id' ], $e->getMessage() );
            }
            
            $old_asset->erase();
        }
        $logger->message( 'CRON', null, 'Ending cron task: hard delete assets.');
        $logger->message( 'CRON', null, "$count assets permanently deleted." );
    }

}