<?php

class Api {

    public function assets_for_market( $f3, $params ) {
        $bucket  = $f3->get( 'aws_bucket' );
        $cdn_url = $f3->get( 'aws_url' );
        $market_id = $params[ 'id' ];

        $asset = new Asset();
        $assets_for_market = $asset->get_asset_details_for_market( $market_id, $bucket, $cdn_url );

        header( 'Content-type: application/json' );
        echo json_encode(
            array( 'assets' => $assets_for_market)
        );
    }

    public function test_google_analytics( $f3 ) {
        $fields = array(
            'email' => 'test@test.com',
            'target_market' => 0
        );
        header('Content-type: application/json');
        echo Google_Analytics::track_event( 'room', 'opened', null, $fields );
    }

    public function test_offline_mode( $f3 ) {
        $region = $f3->get( 'aws_region' );
        $handler = new Amazon_Stack_Handler( $region );
        $result = $handler->updateStack( true );
        var_dump( $result );
    }

}