<?php
/**
 * SETUP
 * 
 * Initialise F3, load classes, set default values, 
 * connect to database, etc.
 */
$DIR = __DIR__;
require( "$DIR/../vendor/autoload.php" );
$f3 = \Base::instance();
// Set environment variables
include( "$DIR/../env.php" );
// Autoload controllers/models/classes
$f3->set( 'AUTOLOAD', "$DIR/../controllers/; $DIR/../models/; $DIR/../util/" );
// Debug (3 = Max)
$f3->set( 'DEBUG', 3);
// Connect to DB
$db_host = $f3->get( 'dbhost' ) . ':' . $f3->get( 'dbport' );
$db_name = $f3->get( 'dbname' );
$db = new \DB\SQL(
    "mysql:host=$db_host;dbname=$db_name",
	$f3->get( 'dbuser' ),
	$f3->get( 'dbpass' )
);
$f3->set( 'DB', $db );

$f3->set( 'SOFT_DELETE_MAX_AGE', 30 );

// Get full host URL (for emailer links)
$base_url = $f3->get( 'dashboard_base_url' );
$manuals_url = "$base_url/presenters/manuals";

date_default_timezone_set( 'Europe/Dublin' );

$assets = new Assets();
$events = new Events();
$manuals = new Manuals();
$teachers = new Teachers();
$admins = new Admins();

$events->spawn_rooms(
    $f3->get( 'emailhost' ),
    $f3->get( 'emailuser' ),
    $f3->get( 'emailpass' ),
    $f3->get( 'reticulum_host' ),
    $f3->get( 'api_host' ),
    $f3->get( 'api_port' ),
    $f3->get( 'api_secret' ),
    $manuals_url,
    $f3->get( 'reticulum_share_url' ),
    $base_url
);

$events->close_rooms(
    $f3->get( 'reticulum_host' ),
    $f3->get( 'reticulum_header_name' ),
    $f3->get( 'reticulum_header_value' )
);

$assets->hard_delete( $f3 );
$events->hard_delete( $f3 );
$manuals->hard_delete( $f3 );
$teachers->hard_delete( $f3 );

$admins->enforce_event_curfew( $f3 );