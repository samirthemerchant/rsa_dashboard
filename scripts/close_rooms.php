<?php
/**
 * SETUP
 * 
 * Initialise F3, load classes, set default values, 
 * connect to database, etc.
 */
$DIR = __DIR__;
require( "$DIR/../vendor/autoload.php" );
$f3 = \Base::instance();
// Set environment variables
include( "$DIR/../env.php" );
// Autoload controllers/models/classes
$f3->set( 'AUTOLOAD', "$DIR/../controllers/; $DIR/../models/; $DIR/../util/" );
// Debug (3 = Max)
$f3->set( 'DEBUG', 3);
// Connect to DB
$db_host = $f3->get( 'dbhost' ) . ':' . $f3->get( 'dbport' );
$db_name = $f3->get( 'dbname' );
$db = new \DB\SQL(
    "mysql:host=$db_host;dbname=$db_name",
	$f3->get( 'dbuser' ),
	$f3->get( 'dbpass' )
);
$f3->set( 'DB', $db );

date_default_timezone_set( 'Europe/Dublin' );

$events = new Events();
$events->close_rooms(
    $f3->get( 'reticulum_host' ),
    $f3->get( 'reticulum_header_name' ),
    $f3->get( 'reticulum_header_value' )
);