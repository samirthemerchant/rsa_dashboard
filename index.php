<?php

/**
 * SETUP
 * 
 * Initialise F3, load classes, set default values, 
 * connect to database, etc.
 */
require( 'vendor/autoload.php' );
$f3 = \Base::instance();

include( 'env.php' );
// Autoload controllers/models/classes
$f3->set( 'AUTOLOAD', 'controllers/; models/; util/' );
// Set default template directory
$f3->set( 'UI', 'views/' );
// Debug (3 = Max)
$f3->set( 'DEBUG', 0);
// Connect to DB
$db = new \DB\SQL(
	'mysql:host=' . $f3->get( 'dbhost' ) . ';dbname=' . $f3->get( 'dbname' ),
	$f3->get( 'dbuser' ),
	$f3->get( 'dbpass' )
);
$f3->set( 'DB', $db );
// Disable template cacheing
$f3->set( 'CACHE', false );
// Use MySQL sessions
$session = new \DB\SQL\Session($db, 'sessions', true, function( \DB\SQL\Session $session, $id ) {
	return true;
} );
$f3->set( 'session', $session );
// Store full host URL (for emailer links)
$scheme  = $f3->get( 'SCHEME' );
$host    = $f3->get( 'HOST' );
$port    = ( $f3->get( 'PORT' ) == '80' || $f3->get( 'PORT' ) == '443' ) ? '' : ':8888';// only for dev
$base    = $f3->get( 'BASE' );
$baseURL = $scheme . '://' . $host . $port . $base;
$f3->set( 'FULL_BASE_URL', $baseURL );
$f3->set( 'SOFT_DELETE_MAX_AGE', 30 );

date_default_timezone_set( 'Europe/Dublin' );

/**
 * ROUTING
 * 
 * Routing URL paths through relevant controllers.
 */
// Error page
$f3->set( 'ONERROR', function( $f3 ) {
	$f3->set( 'title', 'Oh no!' );
	echo \Template::instance()->render( 'error.htm' );   
} );
// Redirect splash page
$f3->route( 'GET /', 'Admins->redirectSplash' );
/** API */
// $f3->route( 'GET @api_market_assets: /api/market/@id', 'Api->assets_for_market' );
// $f3->route( 'GET /api/ga', 'Api->test_google_analytics' );
// $f3->route( 'GET /api/sa', 'Api->test_offline_mode' );
 /** Teachers' Dashboard */
// Teachers dashboard
$f3->route( 'GET  @teacher_home:            /presenters',           'Teachers->home' );
$f3->route( 'GET  @teacher_login:           /presenters/login',     'Teachers->login' );
$f3->route( 'GET  @teacher_logout:          /presenters/logout',    'Teachers->logout' );
$f3->route( 'POST @teacher_check_login:     /presenters/login',     'Teachers->check_login' );
$f3->route( 'GET  @teacher_password:        /presenters/password',  'Teachers->change_password' );
$f3->route( 'POST @teacher_update_password: /presenters/password',  'Teachers->update_password' );
$f3->route( 'GET  @event_lobby:  			/presenters/lobby/@id', 'Teachers->lobby' );

// Teachers' events
$f3->route( 'GET  	 @events:       /presenters/events',            'Events->list' );
$f3->route( 'GET  	 @new_event:    /presenters/events/new',        'Events->form' );
$f3->route( 'POST 	 @create_event: /presenters/events/new',        'Events->save' );
$f3->route( 'GET  	 @edit_event:   /presenters/events/edit/@id',   'Events->form' );
$f3->route( 'POST 	 @update_event: /presenters/events/edit/@id',   'Events->save' );
$f3->route( 'DELETE  @delete_event: /presenters/events/delete/@id', 'Events->delete' );

// Teachers' target markets
$f3->route( 'GET  @markets:       /presenters/markets',          'Markets->list' );
$f3->route( 'GET  @edit_market:   /presenters/markets/edit/@id', 'Markets->edit' );
$f3->route( 'POST @update_market: /presenters/markets/edit/@id', 'Markets->update' );

// Teachers' training manuals
$f3->route( 'GET @manuals: /presenters/manuals', 'Manuals->list' );


/** Admins' Dashboard */
// Admins dashboard
$f3->route( 'GET  @admin_home:            /admins',             'Admins->home' );
$f3->route( 'GET  @admin_login:           /admins/login',       'Admins->login' );
$f3->route( 'GET  @admin_logout:          /admins/logout',      'Admins->logout' );
$f3->route( 'POST @admin_check_login:     /admins/login',       'Admins->check_login' );
$f3->route( 'GET  @admin_password:        /admins/password',    'Admins->change_password' );
$f3->route( 'POST @admin_update_password: /admins/password',    'Admins->update_password' );
$f3->route( 'GET  @admin_close_rooms: 	  /admins/closeActive', 'Admins->force_close_rooms' );
$f3->route( 'GET  @admin_show_2fa:        /admins/login/2fa',   'Admins->show_2fa' );
$f3->route( 'POST @admin_verify_2fa:      /admins/login/2fa',   'Admins->verify_2fa' );
$f3->route( 'GET  @admin_edit_2fa:        /admins/2fa',         'Admins->edit_2fa' );
$f3->route( 'POST @admin_update_2fa:      /admins/2fa',         'Admins->update_2fa' );
$f3->route( 'GET  @admin_reset_2fa:       /admins/2fa/reset',   'Admins->reset_2fa' );


// Admin manage teachers
$f3->route( 'GET  	 @teachers:       /admins/presenters',            'Teachers->list' );
$f3->route( 'GET  	 @new_teacher:    /admins/presenters/new',        'Teachers->new' );
$f3->route( 'POST 	 @create_teacher: /admins/presenters/new',        'Teachers->create' );
$f3->route( 'DELETE  @delete_teacher: /admins/presenters/delete/@id', 'Teachers->delete' );

// Admin manage assets
$f3->route( 'GET  	 @assets:       /admins/assets',            'Assets->list' );
$f3->route( 'GET  	 @new_asset:    /admins/assets/new',        'Assets->form' );
$f3->route( 'POST 	 @create_asset: /admins/assets/new',        'Assets->save' );
$f3->route( 'GET  	 @edit_asset:   /admins/assets/edit/@id',   'Assets->form' );
$f3->route( 'POST 	 @update_asset: /admins/assets/edit/@id',   'Assets->save' );
$f3->route( 'DELETE  @delete_asset: /admins/assets/delete/@id', 'Assets->delete' );

// Admin manage statistics
$f3->route( 'GET  @statistics:       /presenters/statistics',          'Statistics->list' );
$f3->route( 'GET  @edit_statistic:   /presenters/statistics/edit/@id', 'Statistics->edit' );
$f3->route( 'POST @update_statistic: /presenters/statistics/edit/@id', 'Statistics->update' );

// Admin manage training manuals
$f3->route( 'GET  	 @new_manual:    /admins/manuals/new',        'Manuals->form' );
$f3->route( 'POST 	 @create_manual: /admins/manuals/new',        'Manuals->save' );
$f3->route( 'GET  	 @edit_manual:   /admins/manuals/edit/@id',   'Manuals->form' );
$f3->route( 'POST 	 @update_manual: /admins/manuals/edit/@id',   'Manuals->save' );
$f3->route( 'DELETE  @delete_manual: /admins/manuals/delete/@id', 'Manuals->delete' );

$f3->run();