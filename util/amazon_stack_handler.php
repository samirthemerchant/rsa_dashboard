<?php

use Aws\CloudFormation\CloudFormationClient;

class Amazon_Stack_Handler {

    const STACKONLINE_PARAMETER_KEY = 'StackOffline',
          ONLINE_VALUE              = 'Online',
          OFFLINE_VALUE             = 'Offline - Temporarily shut off servers',
          STACK_NAME                = 'cloudsEnteprise',
          EMAIL_ADDRESS             = 'rsavirtualhubs@gmail.com',
          INSTANCE_TYPE             = 't3.medium',
          STREAM_INSTANCE_TYPE      = 't3.medium',
          DOMAIN_NAME               = 'corersa.com',
          KEY_PAIR_NAME             = 'hubsKeyIE';

    private $_client;

    public function __construct( $region ) {
        $this->_client = CloudFormationClient::factory(array(
            'version' => 'latest',
            'region'  => $region,
        ));
    }

    public function updateStack( $online = true ) {
        $result = $this->_client->updateStack( array( 
            'Capabilities' => array(
                'CAPABILITY_IAM',
                'CAPABILITY_NAMED_IAM',
                'CAPABILITY_AUTO_EXPAND',
            ),
            'Parameters' => array(
                array(
                    'ParameterKey' => self::STACKONLINE_PARAMETER_KEY,
                    'ParameterValue' => $online ? self::ONLINE_VALUE : self::OFFLINE_VALUE,
                ),
                array(
                    'ParameterKey' => 'AdminEmailAddress',
                    'ParameterValue' => self::EMAIL_ADDRESS,
                ),
                array(
                    'ParameterKey' => 'AppInstanceCount',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'AppInstanceType',
                    'ParameterValue' => self::INSTANCE_TYPE,
                ),
                array(
                    'ParameterKey' => 'AppPlacementGroupStrategy',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'AutoPauseDb',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'ClassB',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'DatabaseMonthlyBudget',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'DbBackupRetentionPeriod',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'DbMaxCapacity',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'DomainName',
                    'ParameterValue' => self::DOMAIN_NAME,
                ),
                array(
                    'ParameterKey' => 'EmailSubdomain',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'EmailZone',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'InboundCidrOverride',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'InboundSSHCidrOverride',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'InternalZone',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'IsDomainOnRoute53',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'IsDomainOnRoute53',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'KeyPair',
                    'ParameterValue' => self::KEY_PAIR_NAME,
                ),
                array(
                    'ParameterKey' => 'LetsEncryptEmailAddress',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'LoadBalancingMethod',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'MaxStorage',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'NewCMKForDiskEncryption',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'RestoreAppDbSecretArn',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'RestoreBackupVaultName',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'RestoreDbSnapshotIdentifier',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'RestoreRecoveryPointArn',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'RestoreStackName',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'ShortlinkZone',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'SpecifyInboundCidr',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'SpecifyInboundSSHCidr',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'StackOfflineRedirectUrl',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'StreamInstanceCount',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'StreamInstanceType',
                    'ParameterValue' => self::STREAM_INSTANCE_TYPE,
                ),
                array(
                    'ParameterKey' => 'SubnetAZs',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'UnmanagedDomainCertArn',
                    'UsePreviousValue' => true,
                ),
                array(
                    'ParameterKey' => 'UnmanagedDomainEastCertArn',
                    'UsePreviousValue' => true,
                ),
            ),
            'StackName' => self::STACK_NAME,
            'UsePreviousTemplate' => true,
        ) );
        return $result;
    }

}