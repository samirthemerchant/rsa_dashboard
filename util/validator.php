<?php 

class Validator {
	public function getValues( $keys, $params ){
		$values = array();
		foreach ( $keys as $key ) {
			if( isset( $params[ $key ] ) ){
				$values[] = $params[ $key ];
			} else {
				throw new Exception( "Missing parameter: " . $key );
			}
		}
		return $values;
	}
	public function checkEmpty($values){
		foreach ($values as $key => $value) {
			if(trim($value) === ""){
				throw new Exception("Field cannot be empty", 0);
			}
		}
		return;
	}
	public function checkValidJSON($values){
		foreach ($values as $key => $value) {
			if(is_null(json_decode($value))){
				throw new Exception("Invalid JSON in field", 0);
			}
		}
		return;
	}
	public function checkEmail($values){
		foreach ($values as $value) {
			if(!filter_var($value, FILTER_VALIDATE_EMAIL)){
				throw new Exception("Email must be formatted correctly", 0);
			}
		}
		return;
	}
	public function checkNumeric($values){
		foreach ($values as $value) {
			if(!is_numeric($value)){
				throw new Exception("Field must be numeric", 0);
			}
		}
		return;		
	}
	public function checkLength($values){
		foreach ($values as $value) {
			if(strlen(rtrim($value["value"])) != $value["length"]){
				throw new Exception("Field has incorrect length", 0);
			}
		}
		return;		
	}
	public function checkFileErrors($values){
		foreach ($values as $value) {
			if (!isset($value['error']) || is_array($value['error'])){
			    throw new Exception('Invalid file parameters');
			}
			switch ($value['error']) {
			    case UPLOAD_ERR_OK:
			        break;
			    case UPLOAD_ERR_NO_FILE:
			        throw new Exception('No file sent');
			    case UPLOAD_ERR_INI_SIZE:
			    case UPLOAD_ERR_FORM_SIZE:
			        throw new Exception('Exceeded filesize limit');
			    default:
			        throw new Exception('Unknown errors');
			}
		}
		return;
	}
	public function checkFilesize($values, $max, $min = 0){
		foreach ($values as $value) {
			if ($value["size"] > $max){
			    throw new Exception('Exceeded filesize limit');
			}
			if($value["size"] < $min){
				throw new Exception("Filesize too small");
			}
		}
		return;
	}
	public function checkFileType($values, $mimes){
		foreach ($values as $value) {
			$finfo = new finfo(FILEINFO_MIME_TYPE);
			$ext = array_search($finfo->file($value['tmp_name']), $mimes, true);
			if ($ext === false) {
			    throw new Exception('Invalid file format');
			}
		}
		return;
	}

	public function checkUnsafeFileType( $values ) {
		$unsafeFileTypes = array(
			'image/svg+xml',
			'image/svg',
			'application/octet-stream',
			'application/java-archive',
			'text/javascript',
			'application/vnd.apple.installer+xml',
			'application/x-httpd-php',
			'application/x-sh',
			'application/x-shockwave-flash',
		);
		foreach( $values as $value ) {
			$finfo = new finfo(FILEINFO_MIME_TYPE);
			if( false !== array_search( $finfo->file( $value['tmp_name'] ), $unsafeFileTypes, true ) ) {
				throw new Exception( 'Unsafe file type not allowed' );
			}
		}
	}

	public function checkFilepathExists($values){
		foreach ($values as $key => $value) {
			if(!file_exists($value)){
				throw new Exception("Could not find file at: " . $value, 0);
			}
		}
		return;
	}
	private function checkFile($file) {
		if (!isset($file['error']) || is_array($file['error'])) {
		    throw new Exception('Invalid parameters.');
		}

		switch ($file['error']) {
		    case UPLOAD_ERR_OK:
		        break;
		    case UPLOAD_ERR_NO_FILE:
		        throw new Exception('No file sent.');
		    case UPLOAD_ERR_INI_SIZE:
		    case UPLOAD_ERR_FORM_SIZE:
		        throw new Exception('Exceeded filesize limit.');
		    default:
		        throw new Exception('Unknown errors.');
		}

		$finfo = new finfo(FILEINFO_MIME_TYPE);
		if (false === $ext = array_search(
		    $finfo->file($file['tmp_name']),
		    array(
		        'gif' => 'image/gif',
		        'jpg' => 'image/jpeg',
		        'png' => 'image/png',
		        'obj' => 'text/plain',
		        'dae' => 'application/xml',
		        'xml' => 'text/xml',
		        'zip' => 'application/zip'
		    ),
		    true
		)) {
		    throw new Exception('Invalid file format: ' . $finfo->file($file['tmp_name']));
		}

		return;
	}
}
