<?php

class Image_Generator {

    const WIDTH = 1575,
        HEIGHT = 700,
        SIZE = 600;

    private $_font_path,
            $_temp_img_path;

    public function __construct( $font_path ) {
        $this->_font_path = $font_path;
    }

    public function draw( $text ) {
        $img = imagecreatetruecolor( self::WIDTH, self::HEIGHT );
        $black = imagecolorallocate( $img, 0, 0, 0 );
        $white = imagecolorallocate( $img, 255, 255, 255 );

        imagefilledrectangle( $img, 0, 0, 1575, 700, $white );
        imagecolortransparent( $img, $white );
        $bbox = imageftbbox( self::SIZE, 0, $this->_font_path, $text );

        // This is our cordinates for X and Y
        $x = $bbox[ 0 ] + ( imagesx($img) / 2 ) - ( $bbox[ 4 ] / 2 ) - 50;
        $y = $bbox[ 1 ] + ( imagesy($img) / 2 ) - ( $bbox[ 5 ] / 2 ) ;

        imagefttext( $img, self::SIZE, 0, $x, $y, $black, $this->_font_path, $text );
        $this->_temp_img_path = tempnam( sys_get_temp_dir(), 'STAT' );
        imagepng( $img, $this->_temp_img_path );
        imagedestroy( $img );

        return $this->_temp_img_path;
    }

    public function clean( ) {
        unlink( $this->_temp_img_path );
    }
}