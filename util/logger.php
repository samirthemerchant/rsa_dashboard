<?php

class Logger {

	private $_log_file;

	public function __construct($logfile){
		$this->_log_file = fopen($logfile, "a");
	}

	public function message($resource, $id, $message){
        $timestamp = date( 'Y-m-d H:i:s');
        $id_value = is_null( $id ) ? '' : "ID: $id";
		$line = "INFO: $timestamp -- $resource $id_value -- $message\n";
		fwrite($this->_log_file, $line);
		return;
	}

	public function error($resource, $id, $message){
        $timestamp = date( 'Y-m-d H:i:s');
        $id_value = is_null( $id ) ? '' : "ID: $id";
		$line = "ERROR: $timestamp -- $resource $id_value -- $message\n";
		fwrite($this->_log_file, $line);
	}

	public function close(){
		fclose($this->_log_file);
	}

}