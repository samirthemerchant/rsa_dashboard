<?php

use PragmaRX\Google2FA\Google2FA;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;

class Authenticator {

    const COMPANY = 'RSA Core',
        MAX_ATTEMPTS = 5,
        MAX_TIMEOUT = 3600;// 60*60 = 1 hour

    private $_google2fa;

    public function __construct() {
        $this->_google2fa = new Google2FA();
    }

    public function get_qr_code( $user ) {
        if ( is_null( $user->g2fa_secret ) ) {
            $user->g2fa_secret = $this->_google2fa->generateSecretKey();
            $user->save();
        }

        $g2fa_url = $this->_google2fa->getQRCodeUrl(
            self::COMPANY,
            $user->email,
            $user->g2fa_secret
        );
        $writer = new Writer(
            new ImageRenderer(
                new RendererStyle(400),
                new SvgImageBackEnd()
            )
        );

        return $writer->writeString( $g2fa_url );
    }

    public function verify_auth_code( $user, $code ) {
        if ( is_null( $user->g2fa_secret ) ) {
            throw new Exception( "Two-factor authentication has not been configured for this account!" );
        }

        return $this->_google2fa->verifyKey(
            $user->g2fa_secret,
            $code
        );
    }

}