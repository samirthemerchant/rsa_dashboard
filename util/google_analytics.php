<?php

class Google_Analytics {

    const HOST = 'https://www.google-analytics.com',
        VERSION = 1,
        TRACKING_ID = 'UA-181911625-1',
        CLIENT_ID = '7aafa817-b20d-4be0-bd27-a6e8a61cf495',
        HIT_TYPE = 'event';

    private static $CUSTOM_DIMENSIONS = array(
        'email',
        'target_market',
        'location',
        'students',
    );

    public static function track_event( $category, $action, $value = null, $extra_fields = null ) {
        $params = array(
            'v'   => self::VERSION,
            'tid' => self::TRACKING_ID,
            'cid' => self::CLIENT_ID,
            't'   => self::HIT_TYPE,
            'ec'  => $category,
            'ea'  => $action,
            'ua'  => $_SERVER[ 'HTTP_USER_AGENT' ]
        );
        if( ! is_null( $value ) ) {
            $params[ 'el' ] = $value;
        }
        if( ! is_null( $extra_fields ) ) {
            foreach( $extra_fields as $key => $val ) {
                $index = array_search( $key, self::$CUSTOM_DIMENSIONS ) + 1;
                $params[ "cd$index" ] = "$val";
            }
        }

        return self::curl_post( '/collect', $params );
    }

    private static function curl_post( $endpoint, $params ) {
        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, self::HOST . $endpoint );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $params ) );
        
        $response = curl_exec( $ch );
        $http_status_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        curl_close( $ch );
        return $http_status_code;
    }

}