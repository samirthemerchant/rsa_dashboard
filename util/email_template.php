<?php

class Email_Template {

    public static function message( $title, $name, $paragraphs ) {
        $body = self::head( $title );
        $body .= self::greeting( $name );
        foreach( $paragraphs as $p ) {
            if( is_array( $p ) ) {
                $body .= self::table( $p );
            } else {
                $body .= self::paragraph( $p );
            }
        }
        $body .= self::signature();
        $body .= self::foot();
        return $body;
    }

    private static function head( $title ) {
        return '
        <html>
        <head>
            <title>' . $title . '</title>
        </head>
        <body style="width:100%;font-family:Arial,Sans;font-size:1em;color:#58595b;margin:0;padding:0;">
            <div style="width:100%;padding:1em 5em;box-sizing:border-box;">';
    }
    
    private static function greeting( $name ) {
        return '
        <table style="width:100%">
            <tr>
                <td style="text-align:left;vertical-align: bottom;"><h1 style="font-size:1em;font-weight:600;color:#15377a;padding:0;margin:0;">Hi ' . $name . '</h1></td>
            </tr>
        </table>';
    }

    private static function table( $rows ) {
        $table = '<table style="width: 70%;margin-left:auto;margin-right:auto;margin-top:1em;margin-bottom:1em"><tr>';
        
        // Table header
        foreach( $rows[0] as $col ) {
            $table .= '<th style="font-weight: 600;text-align:left">' . $col . '</th>';
        }
        $table .= '</tr>';

        // Table data rows
        foreach( array_slice( $rows, 1 ) as $row ) {
            $table .= '<tr>';
            foreach( $row as $cell ) {
                $table .= '<td style="font-weight: 300;text-align:left">' . $cell . '</td>';
            }
            $table .= '</tr>';
        }

        $table .= '</table>';
        return $table;
    }

    private static function paragraph( $content ) {
        return '
        <p style="width: 70%;font-weight: 300;font-size:1em;line-height:1.1em;text-align: left;margin:1em auto;display:block;padding:0;">'
            . $content . 
        '</p>';
    }

    private static function signature( ) {
        return '
        <p style="width: 70%;font-weight: 300;font-size:1em;line-height:1.1em;text-align: right;margin:1em auto;display:block;padding:0;">
            Thanks<br>The RSA Core Team
        </p>';
    }

    private static function foot( ) {
        return '</div></body></html>';
    }

}