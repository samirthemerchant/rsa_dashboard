<?php 

class Uploader {

	public static function uploadFile($file, $directory) {
		$filename = self::saveFile($file, $directory);
		return $filename;
	}

	public static function saveBase64($base64, $directory) {
			list($base64, $extension) = self::base64Parts($base64);
			$destination = self::createRandomFilename($directory, $extension);

		    $file = fopen($destination, 'wb'); 
		    fwrite($file, base64_decode($base64));
		    fclose($file); 

		    return $destination; 
	}

	public static function reArrayFiles(&$files) {
	    $fileArray 	= array();
	    $count = count($files['name']);
	    $keys 	= array_keys($files);
	    for ($i=0; $i<$count; $i++) {
	        foreach ($keys as $key) {
	            $fileArray[$i][$key] = $files[$key][$i];
	        }
	    }

	    return $fileArray;
	}

	private static function createRandomFilename($directory, $extension) {
		do {
			$filename = md5(uniqid(rand(), true));
			$destination = sprintf('%s/%s.%s', $directory, $filename, $extension);
		} while(file_exists($destination));

		return $destination;
	}

	private static function base64Parts($base64){
		// $data[ 0 ] == "data:image/png;base64"
		// $data[ 1 ] == <actual base64 string>
		list($data, $base64) = explode(',', $base64);
		$dataSplit1 = explode(';', $data);
		$dataSplit2 = explode(':', $dataSplit1[0]);
		$mime = $dataSplit2[1];
		$extension = array_search(
		    $mime,
		    array(
		        'gif' => 'image/gif',
		        'jpg' => 'image/jpeg',
		        'png' => 'image/png'
		    )
		);

		return array($base64, $extension);
	}

	private static function createSafeFilename($directory, $file, $fullTmpPath) {
		$filename = pathinfo($file, PATHINFO_FILENAME);
		$extension = pathinfo($file, PATHINFO_EXTENSION);
		
		if(empty($extension)){
			$finfo = new finfo(FILEINFO_MIME_TYPE);

			$extension = array_search(
			    $finfo->file($fullTmpPath),
			    array(
			        'gif' => 'image/gif',
			        'jpg' => 'image/jpeg',
			        'png' => 'image/png',
			        'obj' => 'text/plain',
			        'fbx' => 'text/plain',
			        'dae' => 'application/xml',
			        'xml' => 'text/xml',
			        'zip' => 'application/zip'
			    )
			);
		}
		
		$filename = str_replace('.', '_', $filename);
		$copy = $filename;

		for($i=1;file_exists($directory.'/'.$copy.'.'.$extension);$i++) {
			$copy = $filename . '-' .$i;
		}

		$filename = $copy;

		return array($filename, $extension);
	}

	private static function saveFile($file, $directory) {
		list($filename, $extension) = self::createSafeFilename($directory, $file['name'], $file['tmp_name']);
		$destination = sprintf('%s/%s.%s', $directory, $filename, $extension);			

		if (!move_uploaded_file(
		    $file['tmp_name'], $destination
		)) {
		    throw new Exception('Could not upload File.');
		}

		return $destination;

	}

}