<?php

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class Amazon_Storage_Handler {

    const ROOT_FOLDER       = 'assets/dashboard/',
          ASSETS_FOLDER     = 'assets/',
          ASSET_KEY_PREFIX  = 'HUB_ASSET_',
          MANUALS_FOLDER    = 'manuals/',
          MANUAL_KEY_PREFIX = 'MANUAL_',
          STAT_KEY_PREFIX   = 'statistics',
          STAT_KEY_SUFFIX   = '.png';

    private $_s3_client, $_region, $_bucket_name;

    public function __construct( $region = null, $bucket_name = null ) {
        $this->_region = $region;
        $this->_bucket_name = $bucket_name;
        if( ! is_null( $region ) ) {
            $this->_s3_client = new S3Client( array(
                'version' => 'latest',
                'region'  => $region
            ) );
        }
    }

    public function get_asset_key_name( $asset_id ) {
        return self::ROOT_FOLDER . self::ASSETS_FOLDER . self::ASSET_KEY_PREFIX . $asset_id;
    }

    public function get_market_key_name( $market_id, $asset_type, $extension ) {
        switch( $extension ) {
            case 'pdf':
                $prefix = 'pdf_';
                break;
            
            case 'mp4':
                $prefix = 'video_';
                break;
        }
        $suffix = ".$extension";
        return self::ROOT_FOLDER . self::ASSETS_FOLDER . $prefix . $market_id . '_' . $asset_type . $suffix;
    }

    public function get_manual_key_name( $manual_id ) {
        return self::ROOT_FOLDER . self::MANUALS_FOLDER . self::MANUAL_KEY_PREFIX . $manual_id;
    }

    public function get_statistic_key_name( $statistic_id ) {
        return self::ROOT_FOLDER . self::ASSETS_FOLDER . self::STAT_KEY_PREFIX . $statistic_id . self::STAT_KEY_SUFFIX;
    }

    public function upload_object( $key_name, $filepath ) {
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $result = $this->_s3_client->putObject( array(
            'Bucket'             => $this->_bucket_name,
            'Key'                => $key_name,
            'Body'               => fopen( $filepath, 'rb' ),
            'ACL'                => 'public-read',
            'ContentDisposition' => 'inline',
            'ContentType'        => $finfo->file( $filepath )
        ) );
        return $result;
    }
        
    public function copy_object( $source_key_name, $dest_key_name ) {
        $bucket = $this->_bucket_name;
        $this->_s3_client->copyObject( array(
            'Bucket'            => $bucket,
            'Key'               => $dest_key_name,
            'CopySource'        => "$bucket/$source_key_name",
            'ACL'               => 'public-read',
            'MetadataDirective' => 'COPY'
        ) );
    }

    public function delete_object( $key_name ) {
        $bucket = $this->_bucket_name;
        $this->_s3_client->deleteObject( array(
            'Bucket' => $bucket,
            'Key'    => $key_name,
        ) );
    }

}