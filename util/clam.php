<?php
// https://www.clamav.net/

class Clam {

    public static function scan( $filepath ) {
        $clamscan = '/usr/local/bin/clamscan';
        $output = shell_exec( "$clamscan $filepath" );
        $lines = explode( "\n", $output );
        $result_parts = explode( ':', $lines[0] );
        $result = trim( $result_parts[1] );
        return 'OK' === $result;
    }
}