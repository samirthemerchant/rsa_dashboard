<?php

class Curl {

    public static function get( $url, $body ) {
        $ch = self::default_curl_init();

        $url .= '?' . http_build_query( $body );
        curl_setopt( $ch, CURLOPT_URL, $url );

        return self::default_curl_response( $ch );
    }

    public static function post( $url, $body, $get_status_and_response = false ) {
        $ch = self::default_curl_init();

        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $body ) );
        
        if( $get_status_and_response ) {
            return self::curl_status_code_and_response( $ch );
        } else {
            return self::default_curl_response( $ch );
        }
    }

    public static function delete( $url, $headers = null ) {
        $ch = self::default_curl_init( $headers );

        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'DELETE' );

        return self::curl_http_status_code( $ch );
    }

    
    private static function default_curl_init( $extra_headers = null ) {
        $ch = curl_init();
        
        $headers = array(
            'Content-type: application/json'
        );
        if( ! empty( $extra_headers ) ) {
            foreach( $extra_headers as $name => $value ) {
                $headers[] = "$name:$value";
            }
        }

        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        return $ch;
    }

    private static function default_curl_response( $ch ) {
        $response = curl_exec( $ch );
        curl_close( $ch );

        return json_decode( $response, true );
    }

    private static function curl_http_status_code( $ch ) {
        $response = curl_exec( $ch );
        $http_status_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        curl_close( $ch );

        return $http_status_code;
    }

    private static function curl_status_code_and_response( $ch ) {
        $response = curl_exec( $ch );
        $http_status_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        curl_close( $ch );

        return array( $http_status_code, $response );
    }
}