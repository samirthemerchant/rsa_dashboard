<?php  

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Emailer {

	const FROM 		  = 'The RSA CORE Team',
		  DATE_FORMAT = 'd/m/Y H:i A';

	private $_host, $_username, $_password, $_static_url;

	public function __construct( $host, $username, $password, $static_url ){
		$this->_host = $host;
        $this->_username  = $username;
        $this->_password  = $password;
        $this->_static_url = $static_url;
	}

	public function welcome_message( $name, $email, $password ){
		$email_title = 'Welcome to RSA CORE';
		
		$paragraphs = array(
			'Welcome to Generation CORE.',
			'Here are your credentials along with your temporary password that you should change as soon as possible.',
			'Email: ' . htmlentities( $email ) . '<br>Password: ' . htmlentities( $password ),
			'<span style="font-weight: 600">Please <a href="' . $this->_static_url . '" target="_blank" style="font-weight: 600;color:#0000EE !important;text-decoration: none;">click here</a> to login into your account!</span>'
		);
		$body = Email_Template::message( $email_title, $name, $paragraphs );

		$this->send( $name, $email, $email_title, $body );
		return;
	}

	public function booking_confirmation( $teacher, $event ) {
		$email_title = 'RSA CORE Booking Confirmation';
		$start = date( self::DATE_FORMAT, strtotime( $event[ 'start_date' ] ) );
		$end   = date( self::DATE_FORMAT, strtotime( $event[ 'end_date' ] ) );
		$event_table = array(
			array( 'Title', 'Target Market', 'Start', 'End'),
			array( $event[ 'title' ], $event[ 'market_name' ], $start, $end )
		);

		if( is_null( $event[ 'share_url' ] ) ) {
			$event_url_paragraph = 'Your unique shareable 3D CORE URL will be sent to you when your room is available.';
		} else {
			$event_url_paragraph = 'This is your unique 3D CORE URL.<br><a href="' . $event[ 'share_url' ] . '" target="_blank" style="font-weight: 600;color:#0000EE !important;text-decoration: none;">' . $event[ 'share_url' ] . '</a>';

		}
		$paragraphs = array(
			'Your 3D CORE booking is now confirmed for <span style="font-weight: 600">' . $event[ 'market_name' ] . '</span>. See further details below.',
			$event_table,
			$event_url_paragraph,
			'<span style="font-weight: 600">The room will be available to enter 24 hours before your event starts.</span>',
			'You can now login to add in your own materials &amp; collateral if needed. See <a href="' . $this->_static_url . '" target="_blank" style="font-weight: 600;color:#0000EE !important;text-decoration: none;">training manual here (ADDING OWN CONTENT)</a> with further info on <span style="font-weight: 600">supported file formats</span> and how to add them to the environment. Otherwise the room has been pre-populated with suitable presentations based on your selected target market.',
			'When you&#39;re satisfied please share the above URL with the school teacher you&#39;re presenting to, who can share with students, and/or project onto a whiteboard via their laptop. Remember to run the performance checklist with them on their machine prior to presentation. See checklist steps here in <a href="' . $this->_static_url . '" target="_blank" style="font-weight: 600;color:#0000EE !important;text-decoration: none;">training manual (EVENT SETUP / CHECKLIST)</a>.'
		);
		$body = Email_Template::message( $email_title, 'Presenter', $paragraphs );

		// if( count( $teachers ) > 1 ) {
			// $this->send_multiple( $teachers, $email_title, $body );
		// } else {
			// $teacher = $teachers[ 0 ];
			$this->send( $teacher->name, $teacher->email, $email_title, $body );
		// }
		return;
	}

	public function event_room_details( $teacher, $event ) {
		$email_title = 'RSA CORE Booking Ready';

		$paragraphs = array(
			'Your 3D CORE booking for <span style="font-weight: 600">' . $event[ 'market_name' ] . '</span> is now ready. See further details below.',
			'This is your unique 3D CORE URL.<br><a href="' . $event[ 'share_url' ] . '" target="_blank" style="font-weight: 600;color:#0000EE !important;text-decoration: none;">' . $event[ 'share_url' ] . '</a>',
			'You can now login to add in your own materials &amp; collateral if needed. See <a href="' . $this->_static_url . '" target="_blank" style="font-weight: 600;color:#0000EE !important;text-decoration: none;">training manual here</a> with further info on <span style="font-weight: 600">supported file formats</span> and how to add them to environment. Otherwise room has been pre-populated with suitable presentations based on your selected target market.',
			'When you&#39;re satisfied please <span style="font-weight: 600">share above URL with the school teacher</span> you&#39;re presenting to, who can share with students, or if only school teacher accessing HUB on the day and projecting onto whiteboard via their laptop, make sure to run the <span style="font-weight: 600">performance checklist</span> with them on their machine prior to presentation. <a href="' . $this->_static_url . '" target="_blank" style="font-weight: 600;color:#0000EE !important;text-decoration: none;">See checklist steps here in training manual</a>.',
			'Best of luck.'
		);
		$body = Email_Template::message( $email_title, 'Presenter', $paragraphs );

		// if( count( $teachers ) > 1 ) {
			// $this->send_multiple( $teachers, $email_title, $body );
		// } else {
			// $teacher = $teachers[ 0 ];
			$this->send( $teacher->name, $teacher->email, $email_title, $body );
		// }
		return;
	}

	private function send( $name, $email, $subject, $body ) {
		$mail = $this->default_mailer_settings( $subject, $body );
	    $mail->addAddress( $email, $name ); // Add a recipient
	    $mail->send();
	    return;
	}
	private function send_multiple( $users, $subject, $body ) {
		$mail = $this->default_mailer_settings( $subject, $body );
	    $mail->addAddress( $this->_username, self::FROM ); // Add a recipient (same as sender)
		foreach( $users as $user ) {
			$mail->addBCC( $user[ 'email' ], $user[ 'name' ] );
		} 
	    $mail->send();
	    return;
	}

	private function default_mailer_settings( $subject, $body ) {
		$mail = new PHPMailer( true ); // Passing `true` enables exceptions
        // SMTP::DEBUG_OFF = off (for production use)
        // SMTP::DEBUG_CLIENT = client messages
        // SMTP::DEBUG_SERVER = client and server messages
	    $mail->SMTPDebug = SMTP::DEBUG_OFF;
	    $mail->IsSMTP(); // enable SMTP
	    $mail->Host       = $this->_host;
	    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; // secure transfer enabled REQUIRED for Gmail
	    $mail->Port       = 587; // 465 / 587
	    $mail->SMTPAuth   = true; // authentication enabled
	    $mail->Username   = $this->_username; // SMTP username
	    $mail->Password   = $this->_password; // SMTP password
	    $mail->IsHTML( true );
	    $mail->CharSet    = 'UTF-8';  
	    $mail->SetFrom( $this->_username, self::FROM );
	    $mail->Subject    = $subject;
	    $mail->Body       = $body;
		return $mail;
	}

	// private function welcomeTemplate( $name, $email, $password ){
	// 	return '
	// 	<html>
	// 		<head>
	// 			<title>Welcome to RSA Hubs</title>
	// 		</head>
	// 		<body style="width:100%;font-family:Arial,Sans;font-size:1em;color:#58595b;margin:0;padding:0;">
	// 			<div style="width:100%;padding:1em 5em;box-sizing:border-box;">
	// 				<table style="width:100%">
	// 					<tr>
	// 						<td style="text-align:left;vertical-align: bottom;"><h1 style="font-size:1em;font-weight:600;color:#15377a;padding:0;margin:0;">Dear ' . $name . '</h1></td>
	// 					</tr>
	// 				</table>
	// 				<p style="width: 70%;font-weight: 300;font-size:1em;line-height:1.1em;text-align: left;margin:1em auto;display:block;padding:0;">
	// 					Welcome to Generation Hubs.
	// 				</p>
	// 				<p style="width: 70%;font-weight: 300;font-size:1em;line-height:1.1em;text-align: left;margin:1em auto;display:block;padding:0;">
	// 					Here are your credentials along with your temporary password that you should change as soon as possible.
	// 				</p>
	// 				<p style="width: 70%;font-weight: 300;font-size:1em;line-height:1.1em;text-align: left;margin:1em auto;display:block;padding:0;">
	// 					Email: ' . htmlspecialchars($email) . '<br>
	// 					Password: ' . htmlspecialchars($password) . '
	// 				</p>
	// 				<p style="width: 70%;font-weight: 300;font-size:1em;line-height:1.1em;text-align: left;margin:1em auto;display:block;padding:0;">
	// 					<span style="font-weight: 600">Please <a href="' . $this->_login_url . '" target="_blank" style="font-weight: 600;color:#0000EE !important;text-decoration: none;">click here</a> to login into your account!</span>
	// 				</p>
	// 				<p style="width: 70%;font-weight: 300;font-size:1em;line-height:1.1em;text-align: right;margin:1em auto;display:block;padding:0;">
	// 					Thanks!
	// 				</p>
	// 			</div>
	// 		</body>
	// 	</html>';
	// }

}